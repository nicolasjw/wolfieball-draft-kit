/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wolfieballdraftkit;

/**
 *
 * @author Nicolas
 */
public enum WBDK_PropertyType {
    //LOADED FROM properties.xml
    PROP_APP_TITLE,
    
    //APPLICATION ICONS   
    NEW_DRAFT_ICON,
    LOAD_DRAFT_ICON,
    SAVE_DRAFT_ICON,
    SAVEAS_DRAFT_ICON,
    EXPORT_DRAFT_ICON,
    EXIT_DRAFT_ICON,
    PLAYERS_ICON,
    FANTASY_TEAMS_ICON,
    FANTASY_STANDINGS_ICON,
    DRAFT_ICON,
    MLB_TEAMS_ICON,
    ADD_ICON,
    MINUS_ICON,
    EDIT_ICON,
    
    //APPLICATION TOOLTIPS
    NEW_DRAFT_TOOLTIP,
    LOAD_DRAFT_TOOLTIP,
    SAVE_DRAFT_TOOLTIP,
    EXPORT_DRAFT_TOOLTIP,
    EXIT_DRAFT_TOOLTIP,
    PLAYERS_TOOLTIP,
    FANTASY_TEAMS_TOOLTIP,
    FANTASY_STANDINGS_TOOLTIP,
    DRAFT_TOOLTIP,
    MLB_TOOLTIP,
    ADD_PLAYER_TOOLTIP,
    REMOVE_PLAYER_TOOLTIP,
    ADD_FANTASY_TEAM_TOOLTIP,
    REMOVE_FANTASY_TEAM_TOOLTIP,
    EDIT_FANTASY_TEAM_TOOLTIP,
    
    
    // AND VERIFICATION MESSAGES
    NEW_DRAFT_CREATED_MESSAGE,
    DRAFT_LOADED_MESSAGE,
    DRAFT_SAVED_MESSAGE,
    SITE_EXPORTED_MESSAGE,
    SAVE_UNSAVED_WORK_MESSAGE,
    REMOVE_ITEM_MESSAGE    
    
}
