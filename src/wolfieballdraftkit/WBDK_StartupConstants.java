package wolfieballdraftkit;

/**
 * This class stores all the constants used by the Course Site Builder application 
 * at startup, which means before the user interface is even loaded. This mostly 
 * means how to find files for initializing the application, like properties.xml.
 * 
 * @author Nicolas
 */
public class WBDK_StartupConstants {
    
    //WE NEED THESE CONTANTS IN ORDER TO START THE PROGRAM PROPERLY
    public static final String PROPERTIES_FILE_NAME = "properties.xml";
    public static final String PROPERTIES_SCHEMA_FILE_NAME = "properties_schema.xsd";
    public static final String PATH_CSS = "csb/css/";    
    public static final String PATH_DATA = "./data/";
    public static final String PATH_IMAGES = "./images/";
    public static final String PATH_PLAYER_PHOTOS = PATH_IMAGES + "players/";
    public static final String PATH_FLAGS = PATH_IMAGES + "flags/";    
    public static final String PATH_EMPTY = ".";
    public static final String JSON_FILE_PATH_HITTERS = PATH_DATA + "Hitters.json";
    public static final String JSON_FILE_PATH_PITCHERS = PATH_DATA + "Pitchers.json";
    
    // ERROR DIALOG CONTROL
    public static String CLOSE_BUTTON_LABEL = "Close";    
}
