/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wolfieballdraftkit;

import java.io.IOException;
import static wolfieballdraftkit.WBDK_StartupConstants.*;
import static wolfieballdraftkit.WBDK_PropertyType.*;
import javafx.application.Application;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import wbdk.gui.WBDK_GUI;
import xml_utilities.InvalidXMLFileFormatException;

/**
 *
 * @author Nicolas
 */
public class WolfieballDraftKit extends Application {
    
    WBDK_GUI gui;
    
    @Override
    public void start(Stage primaryStage) {
       
        
        //TRY TO LOAD APP
        boolean success = loadProperties();
        if(success) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String appTitle = props.getProperty(PROP_APP_TITLE);
        
            gui = new WBDK_GUI(primaryStage);
            gui.initGui("");
        }
        
    }
    
        /**
     * Loads this application's properties file, which has a number of settings
     * for initializing the user interface.
     * 
     * @return true if the properties file was loaded successfully, false otherwise.
     */
    public boolean loadProperties() {
        try {
            // LOAD THE SETTINGS FOR STARTING THE APP
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            props.addProperty(PropertiesManager.DATA_PATH_PROPERTY, PATH_DATA);
            props.loadProperties(PROPERTIES_FILE_NAME, PROPERTIES_SCHEMA_FILE_NAME);
            return true;
       } catch (InvalidXMLFileFormatException ixmlffe) {
            return false;
        }        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
