/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk.controller;

import static wolfieballdraftkit.WBDK_StartupConstants.*;
import static wolfieballdraftkit.WBDK_PropertyType.*;
import java.io.IOException;
import properties_manager.PropertiesManager;
import wbdk.gui.MessageDialog;
import wbdk.gui.WBDK_GUI;
import wbdk.gui.YesNoCancelDialog;

/**
 *
 * @author Nicolas
 */
public class FileController {
    
    //WANNA SEE IF SOMETHING HAS BEEN SAVED OR NOT
    private boolean saved;
    
    //HOW TO READ AND WRITE DATA
    //DATA MANAGER GOES HERE
    
    //ASK YES NO CANCEL QUESTIONS
    YesNoCancelDialog yesNoCancelDialog;
    
    //THIS WILL PROVIDE FEEDBACK TO THE USER
    //AFTER WORK BY THIS CLASS HAS BEEN COMPLETED
    MessageDialog messageDialog;
    
    //USE THIS FOR VERIFICATION FEEDBACK
    PropertiesManager properties;
    
    public FileController(MessageDialog initMessageDialog, 
            YesNoCancelDialog initYesNoCancelDialog) {
        
        //Nothing yet
        saved = true;
        
        //GET PROVIDED FEEDBACK
        messageDialog = initMessageDialog;
        yesNoCancelDialog = initYesNoCancelDialog;
        properties = PropertiesManager.getPropertiesManager();
        
    }
    
    public void markAsEdited(WBDK_GUI gui) {
        
    }
    
    public void handleNewDraftRequest(WBDK_GUI gui) {
        try {
            //MAY HAVE TO SAVE CURRENT WORK
            boolean continueToMakeNew = true;
            if(!saved) {
                //USER CHOOSES TO CANCEL
                continueToMakeNew = promptToSave(gui);
            }
            
            //IF USER EALLY WANTS TO MAKE A NEW COURSE
            if(continueToMakeNew) {
                //RESET ALL DEFAULT DATA
                //reset here
                saved = false;
                
                //REFRESH GUI
                //refresh here
                
                //TELL THE USER THE COURSE HAS BEEN CREATED
                messageDialog.show(properties.getProperty(NEW_DRAFT_CREATED_MESSAGE));
            }
            //ONLY DRAW THE PANE TOOLBAR WHEN A NEW DRAFT IS CREATED
            gui.drawFileToolbar();
            gui.paintMainPane();
            //MAKE STAGE RED
            
        }catch(IOException e) {
            
        }
    }
    
    public void handleLoadDraftRequest(WBDK_GUI gui) {
        
    }    
    
    public void handleSaveRequest(WBDK_GUI gui) {
        
    }    
    
    public void handleExportDraftRequest(WBDK_GUI gui) {
        
    }   
    
    public void handleExitDraftRequest(WBDK_GUI gui) {
        try {
            //MAY HAVE TO SAVE CURRENT DATE
            boolean continueToExit = true;
            if(!saved) {
                //THE USER CAN OPT OUT
                continueToExit = promptToSave(gui);
            }
            
            //IF THE USER REALLY WANTS TO EXIT
            if(continueToExit) {
                //ExIT
                System.exit(0);
            }            
        }catch(IOException e) {
            
        }
    }    
    
    public boolean promptToSave(WBDK_GUI gui) throws IOException {
        
        //PROMPT USER TO SAVE UNSAVED WORK
        yesNoCancelDialog.show(properties.getProperty(SAVE_UNSAVED_WORK_MESSAGE));
        
        //GET USERS SELECTION
        String selection = yesNoCancelDialog.getSelection();
        
        //IF USER SAID YES, THEN SAVE BEFORE MOVING ON
        if(selection.equals(YesNoCancelDialog.YES)) {
            // SAVE DRAFt
            //we will save the draft here
            saved = true;
        }
        
        //IF THE USER SAID CANCEL, THEN WE'LL TELL WHOEVER
        //CALLED THIS THAT THE USER IS NOT INTERESTED ANYMORE
        else if (selection.equals(YesNoCancelDialog.CANCEL)) {
            return false;
        }
        
        //IF USER SAID NO, CONTINUE WITHOUT SAVING
        return true;
    }
}
