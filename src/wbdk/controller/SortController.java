/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.scene.control.TableView;
import wbdk.data.DataManager;
import wbdk.data.Player;

/**
 *
 * @author Nicolas
 */
public class SortController {
    
    DataManager dataManager = DataManager.getDataManager();
    ObservableMap<String, ObservableList<Player>> map = dataManager.getSortPlayersMap();
    ObservableList<Player> current;
    
    public void handleAllSort(TableView table) {
        current = map.get("ALL");
        table.setItems(current);
    }
    
    public void handleCSort(TableView table) {
        current = map.get("C");
        table.setItems(current);
    }
    
    public void handle1BSort(TableView table) {
        current = map.get("1B");
        table.setItems(current);
    }
    
    public void handleClSort(TableView table) {
        current = map.get("Cl");
        table.setItems(current);
    }
    
    public void handle3BSort(TableView table) {
        current = map.get("3B");
        table.setItems(current);
    }
        
    public void handle2BSort(TableView table) {
        current = map.get("2B");
        table.setItems(current);
    }
            
    public void handleMlSort(TableView table) {
        current = map.get("Ml");
        table.setItems(current);
    }
    
    public void handleSSSort(TableView table) {
        current = map.get("SS");
        table.setItems(current);
    }
    
    public void handleOFSort(TableView table) {
        current = map.get("OF");
        table.setItems(current);
    }
        
    public void handleUSort(TableView table) {
        current = map.get("U");
        table.setItems(current);
    }
    
    public void handlePSort(TableView table) {
        current = map.get("P");
        table.setItems(current);
    }
    
    
    public ObservableList getCurrent() {
        return current;
    }
    
    public void handleSearchRequest(TableView<Player> table, String newValue) {
        dataManager.putPlayersIntoMap(dataManager.getPlayers());
        ObservableList<Player> players = dataManager.getPlayers(); //ALWAYS SEARCH THE AVAILABLE PLAYERS
        ObservableList<Player> temp = FXCollections.observableArrayList(); //CREATE A TEMP LIST TO HOLD ALL PLAYERS THAT MATCH CRITERIA
        for(int i = 0; i < players.size(); i++) { //ITERATE THROUGH ALL AVAILABLE PLAYERS AND CHECK NAMES
            if(players.get(i).getFirstName().startsWith(newValue) 
                    || players.get(i).getLastName().startsWith(newValue)) { //IF NAME MATCHES CRITERIA, ADD IT TO THE TEMP LIST
                temp.add(players.get(i));
            }
        }
        dataManager.putPlayersIntoMap(temp); //THIS WILL CREATE A HASHMAP WITH ONLY THE PEOPLE THAT MATCH THE SEARCH CRITERIA
        table.setItems(temp);
    }
        
                
        
    
}
