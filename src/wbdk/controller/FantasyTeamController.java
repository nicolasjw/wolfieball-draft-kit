/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk.controller;

import javafx.collections.ObservableList;
import javafx.stage.Stage;
import wbdk.data.DataManager;
import wbdk.data.FantasyTeam;
import wbdk.data.Player;
import wbdk.gui.AddFantasyTeamDialog;
import wbdk.gui.FantasyTeamItemDialog;
import wbdk.gui.MessageDialog;
import wbdk.gui.YesNoCancelDialog;

/**
 *
 * @author Nicolas
 */
public class FantasyTeamController {
    
    AddFantasyTeamDialog aftd;
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;
    FantasyTeamItemDialog ftid;
    DataManager dataManager = DataManager.getDataManager();
    
    public FantasyTeamController(Stage initPrimaryStage, MessageDialog initMessageDialog,
            YesNoCancelDialog initYesNoCancelDialog, FantasyTeam teamToEdit) {
        aftd = new AddFantasyTeamDialog(initPrimaryStage, messageDialog);
        yesNoCancelDialog = initYesNoCancelDialog;
        ftid = new FantasyTeamItemDialog(initPrimaryStage, teamToEdit);
    }
    
    public void handleAddFantasyTeamRequest() {
        aftd.showAddFantasyTeamDialog();
        if(aftd.wasCompleteSelected() == true) {
            FantasyTeam teamToAdd = aftd.getFantasyTeam();
            dataManager.getFantasyTeams().add(teamToAdd);
        }
        else {
            //do nothing because it wasn't completed or canceled
        }
    }
    
    public void handleRemoveFantasyTeamRequest(String teamToRemove, int indexOfTeam) {
        yesNoCancelDialog.show("Are you sure you wish to remove " + teamToRemove + 
                " from the draft?");
        if(yesNoCancelDialog.getSelection().equals("Yes")) {
            //MOVE ALL PLAYERS INTO THE THE AVAIALABLE PLAYERS POOL
            FantasyTeam teamToBeRemoved = (FantasyTeam)dataManager.getFantasyTeams().get(indexOfTeam);
            for(int i = 0; i < teamToBeRemoved.getStartingPlayer().size(); i++) {
                dataManager.getPlayers().add(teamToBeRemoved.getStartingPlayer().get(i));
            }
            dataManager.getFantasyTeams().remove(indexOfTeam);
        }
        else {
            //do nothing because they hit no or cancel
        }
    }
    
    public void handleEditFantasyTeamRequest (FantasyTeam teamToEdit) {
        ftid.showFantasyTeamItemDialog(teamToEdit);
        if(ftid.wasCompleteSelected()) {
            FantasyTeam edittedTeam = ftid.getFantasyTeam();
            teamToEdit.setName(teamToEdit.getName());
            teamToEdit.setOwner(teamToEdit.getOwner());
        }
        else {
            //do nothing
        }
    }
    
}
