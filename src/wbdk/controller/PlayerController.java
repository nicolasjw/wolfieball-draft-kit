/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk.controller;

import java.util.ArrayList;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import wbdk.data.DataManager;
import wbdk.data.FantasyTeam;
import wbdk.data.Hitter;
import wbdk.data.Player;
import wbdk.gui.AddPlayerDialog;
import wbdk.gui.MessageDialog;
import wbdk.gui.PlayerItemDialog;
import wbdk.gui.YesNoCancelDialog;

/**
 *
 * @author Nicolas
 */
public class PlayerController {
    
    AddPlayerDialog apd;
    PlayerItemDialog pid;
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;
    DataManager dataManager = DataManager.getDataManager();
    
    public PlayerController(Stage initPrimaryStage, Player player, MessageDialog initMessageDialog,
            YesNoCancelDialog initYesNoCancelDialog) {
        pid = new PlayerItemDialog(initPrimaryStage, player, initMessageDialog);
        apd = new AddPlayerDialog(initPrimaryStage, messageDialog);
        messageDialog = initMessageDialog;
        yesNoCancelDialog = initYesNoCancelDialog;
    }
    
    public void handlePlayerAddController() {
        apd.showAddPlayerDialog();
        if(apd.wasCompleteSelected() == true) {
            Player playerToAdd = apd.getPlayer();
            dataManager.getPlayers().add(playerToAdd);
        }
        else {
            //do nothing because it wasn't completed or canceled
        }
    }
    
    public void handlePlayerRemoveController(Player player) {
        yesNoCancelDialog.show("Are you sure you wish to remove " + player.getFirstName() + " " + player.getLastName() + " from the list?");
        if(yesNoCancelDialog.getSelection().equals("Yes")){
            dataManager.getPlayers().remove(player);        
        }
        else {
            //do nothing because they hit no or cancel
        }
    }
    
    public void handlePlayerEditController(TableView table, Player playerToEdit, TableView availablePlayers) {
        pid.showEditScheduleItemDialog(playerToEdit);
        
        if(pid.wasCompleteSelected()) {
            //UPDATE PLAYER
            Player edittedPlayer = pid.getPlayer();
            if(dataManager.getPlayers().contains(edittedPlayer)) {
                dataManager.getPlayers().remove(edittedPlayer);
            }
            //ADD HIM TO THE APPROPRIATE TEAM]
            String teamName = pid.getFantasyTeamName();
            
            //FIND THE TEAM
            ArrayList<Player> tempPlayers = new ArrayList(23);            
            for(int i = 0; i < dataManager.getFantasyTeams().size(); i++) {
                FantasyTeam ft = (FantasyTeam)dataManager.getFantasyTeams().get(i);
                //REMOVE PLAYERS POSITION FROM THE AVAILABLE POSITIONS
                int j;
                for(j = 0; j < ft.getOpenPositions().length; j++) {
                    if(edittedPlayer.getChosenPosition().equals(ft.getOpenPositions()[j])) {
                        ft.getOpenPositions()[j] = "";
                        break;
                    }
                }
                if(ft.getStartingPlayer().size() == 0) {
                    for(int k = 0; k < 23; k++) {
                        tempPlayers.add(new Player());
                    }
                    ft.getStartingPlayer().setAll(tempPlayers);
                }                
                if(teamName.equals(ft.getName())) {
                    ft.getStartingPlayer().set(j, edittedPlayer);
                    dataManager.getPitchers().remove(edittedPlayer);
                    dataManager.getHitters().remove(edittedPlayer);
                    availablePlayers.getItems().remove(edittedPlayer);
                    break;
                }
                else if(teamName.equals("Free Agents")) {
                    for(int z = 0; z < 23; z++) {
                       Player tempPlayer = (Player)ft.getStartingPlayer().get(z);
                       if(tempPlayer == null) 
                           continue;
                       if(edittedPlayer.getFirstName().equals(tempPlayer.getFirstName()) && 
                               edittedPlayer.getLastName().equals(tempPlayer.getLastName()) &&
                               edittedPlayer.getChosenPosition().equals(tempPlayer.getChosenPosition())){
                            dataManager.getPlayers().add(edittedPlayer);
                            dataManager.reloadPitchersAndHitters();
                            //RESET AVAILABLE POS
                            ft.getOpenPositions()[z] = tempPlayer.getChosenPosition();
                            break;
                        }
                    }
                }
            }
            //REMOVE PLAYER FROM CURRENT AVAILABLE LIST
            //REMOVE PLAYER FROM CURRENT AVAILABLE LIST
            String teamToRemoveFrom = pid.getFantasyTeamNamePlayerWasOn();
            for(int i = 0; i < dataManager.getFantasyTeams().size(); i++) {
                FantasyTeam team = (FantasyTeam)dataManager.getFantasyTeams().get(i);
                if(team.getName().equals(teamToRemoveFrom)) {
                    for(int j = 0; j < 23; j++) {
                        Player p = (Player)team.getStartingPlayer().get(j);
                        if(p.getFirstName().isEmpty()) {
                            continue;
                        }
                        else if(p.getLastName().equals(edittedPlayer.getLastName()) && p.getFirstName().equals(edittedPlayer.getFirstName())) {
                            table.getItems().set(j, new Player());
                            //table.getItems().get(i);
                        }
                }
            }            
        }            
        }
        else {
            //do nothing
        }
        
        dataManager.updateTeamStats();
        dataManager.updatePoints();
        dataManager.reloadPitchersAndHitters();
        dataManager.updatePlayerValues();
    }
        
}
