/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk.controller;

import javafx.scene.control.ScrollPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import wbdk.gui.WBDK_GUI;

/**
 *
 * @author Nicolas
 */
public class ScreenController {
    
    
    public void handlePlayerScreenRequest(WBDK_GUI gui) {
        
        //SET THE MAIN PANES CENTER TO BE THE PLAYER PANE
        BorderPane pane = gui.getMainPane();
        VBox playerPane = gui.getPlayersVBox();
        pane.setCenter(playerPane);
    }
    
    public void handleFantasyTeamsScreenRequest(WBDK_GUI gui) {

        //SET THE MAIN PANES CENTER TO BE THE FANTASY TEAMS PANE
        BorderPane pane = gui.getMainPane();
        VBox fantasyTeamsPane = gui.getFantasyTeamsVBox();
        pane.setCenter(fantasyTeamsPane);                
    }
    
    public void handleFantasyStandingsScreenRequest(WBDK_GUI gui) {

        //SET THE MAIN PANES CENTER TO BE THE FANTASY STANDINGS PANE
        BorderPane pane = gui.getMainPane();
        VBox fantasyStandings = gui.getFantasyStandingsVBox();
        pane.setCenter(fantasyStandings);        
    }
    
    public void handleDraftScreenRequest(WBDK_GUI gui) {
        
        //SET THE MAIN PANES CENTER TO BE THE DRAFT PANE
        BorderPane pane = gui.getMainPane();
        VBox draft = gui.getDraftVBox();
        pane.setCenter(draft);                
    }
    
    public void handleMLBTeamsScreenRequest(WBDK_GUI gui) {
        
        //SET THE MAIN PANES CENTER TO BE THE MLB TEAMS PANE
        BorderPane pane = gui.getMainPane();
        VBox mlbTeams = gui.getMLBTeamsVBox();
        pane.setCenter(mlbTeams);                
    }    
    
}
