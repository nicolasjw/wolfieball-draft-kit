/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk.file;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;

/**
 *
 * @author Nicolas
 */
public class JSONFileManager {
    
    //JSON FILE READING AND WRITING CONSTANTS
    String JSON_TEAM = "TEAM";
    String JSON_LAST_NAME = "LAST_NAME";
    String JSON_FIRST_NAME = "FIRST_NAME";
    String JSON_QUALIFYING_POS = "QP";
    String JSON_AB = "AB";  //WHAT IS THIS?
    String JSON_RUNS = "R";
    String JSON_HITS = "H";
    String JSON_HOMERUNS = "HR";
    String JSON_RBI = "RBI";
    String JSON_SB = "SB";
    String JSON_NOTES = "NOTES";
    String JSON_YEAR_OF_BIRTH = "YEAR_OF_BIRTH";
    String JSON_NATION_OF_BIRTH = "NATION_OF_BIRTH";
    String JSON_IP = "IP"; //WTF
    String JSON_ER = "ER"; //WTF
    String JSON_WALKS = "W";
    String JSON_SAVES = "SV";
    String JSON_H = "H"; //WTF
    String JSON_BB = "BB"; //WTF
    String JSON_STRIKEOUTS = "K";
    
    private static JSONFileManager singleton;
    
    public static JSONFileManager getJSONFileManager(){
        if(singleton == null) {
            singleton = new JSONFileManager();
        }
        return singleton;
    }
    
    public JsonObject loadJSONFile(String filePath) throws IOException {
        InputStream is = new FileInputStream(filePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }
    
    public ArrayList<String> loadArrayFromJSONFile(String jsonFilePath, String arrayName) throws IOException {
        JsonObject json = loadJSONFile(jsonFilePath);
        ArrayList<String> items = new ArrayList();
        JsonArray jsonArray = json.getJsonArray(arrayName);
        for (JsonValue jsV : jsonArray) {
            items.add(jsV.toString());
        }
        return items;
    }
    
}
