/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk.gui;
import java.io.IOException;
import java.util.Comparator;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.geometry.Insets;
import static wolfieballdraftkit.WBDK_StartupConstants.*;
import wolfieballdraftkit.WBDK_PropertyType;
import properties_manager.PropertiesManager;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import wbdk.controller.DraftController;
import wbdk.controller.FantasyTeamController;
import wbdk.controller.FileController;
import wbdk.controller.PlayerController;
import wbdk.controller.ScreenController;
import wbdk.controller.SortController;
import wbdk.data.DataManager;
import wbdk.data.FantasyTeam;
import wbdk.data.Hitter;
import wbdk.data.Pitcher;
import wbdk.data.Player;
import wbdk.data.Team;

/**
 *
 * @author Nicolas
 */
public class WBDK_GUI {
    
    final String COL_FIRST = "First";
    final String COL_LAST = "Last";
    final String COL_TEAM = "Pro Team";
    final String COL_POS = "Positions";
    final String COL_DOB = "Year of Birth";
    final String COL_RUNS = "R";
    final String COL_WALKS = "W";
    final String COL_HR = "HR";
    final String COL_SV = "SV";
    final String COL_RBI = "RBI";
    final String COL_K = "K";
    final String COL_SB = "SB";
    final String COL_ERA = "ERA";
    final String COL_BA = "BA";
    final String COL_WHIP = "WHIP";
    final String COL_VALUE = "Estimated Value";
    final String COL_NOTES = "Notes";
    
    //Controllers
    FileController fileController;
    ScreenController screenController;
    SortController sortController;
    PlayerController playerController;
    FantasyTeamController fantasyTeamsController;
    DraftController draftController;
    DataManager dataManager = DataManager.getDataManager();
    
    //DIALOGS
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;
    
    //MISC GUI 
    Screen screen;
    Stage primaryStage;
    Scene mainScene;
    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "wbdk_style.css";    

    
    //DIFFERENT PANES
    BorderPane mainPane;
    VBox playersPane;
    VBox fantasyTeamsPane;
    VBox fantasyStandingsPane;
    VBox draftPane;
    VBox mlbTeamsPane;
    
    //TOOL BARS AND TOOL BAR BUTTONS
    HBox fileToolbar;
    Button newDraft;
    Button loadDraft;
    Button saveDraft;
    Button savaAsDraft;
    Button exportDraft;
    Button exitDraft;
    HBox paneToolbar;
    Button playersButton;
    Button fantasyTeamsButton;
    Button fantasyStandingsButton;
    Button draftButton;
    Button mlbTeamsButton;
    
    //MISC BUTTONS
    Button addPlayer;
    Button removePlayer;
    
    //MISC PLAYER PAGE ELEMENTS
    HBox buttonsHBox;
    HBox sortHBox;
    Label searchLabel;
    TextField searchBox;
    final ToggleGroup group = new ToggleGroup();
    RadioButton all;
    RadioButton C;
    RadioButton firstBase;
    RadioButton CL;
    RadioButton secondBase;
    RadioButton thirdBase;
    RadioButton MI;
    RadioButton SS;
    RadioButton OF;
    RadioButton U;
    RadioButton P;
    
    //PLAYER TABLE
    TableView<Player> availablePlayers;
    TableColumn firstName;
    TableColumn lastName;
    TableColumn proTeam;
    TableColumn positions;
    TableColumn dob;
    TableColumn runs;
    TableColumn walks;
    TableColumn homeruns;
    TableColumn saves;
    TableColumn rbi;
    TableColumn strikeouts;
    TableColumn stolenBases;
    TableColumn era;
    TableColumn battingAvg;
    TableColumn whip;
    TableColumn estimatedValue;
    TableColumn notes;
    
    //MISC FANTASY TEAMS PAGE ELEMENTS
    HBox fantasyTeamsButtonsHBox;
    Button addFantasyTeam;
    Button removeFantasyTeam;
    Button editFantasyTeam;
    Label selectFantasyTeamLabel;
    Label startingLineupLabel;
    Label taxiSquadLabel;
    ComboBox fantasyTeamsComboBox;
    VBox startingVBox;
    VBox taxiVBox;
    
    //FANTASY TEAMS TABLE
    //STARTING
    TableView<Player> startingFantasyTeam;
    TableColumn startingFtPos;
    TableColumn startingFtFirstName;
    TableColumn startingFtLastName;
    TableColumn startingFtProTeam;
    TableColumn startingFtPositions;
    TableColumn startingFtRuns;
    TableColumn startingFtWalks;
    TableColumn startingFtHomeruns;
    TableColumn startingFtSaves;
    TableColumn startingFtRBI;
    TableColumn startingFtStrikeout;
    TableColumn startingFtStolenBases;
    TableColumn startingFtERA;
    TableColumn startingFtBattingAvg;
    TableColumn startingFtWHIP;
    TableColumn startingFtEstimatedValue;
    TableColumn startingFtcontract;    
    //TAXI
    TableView<Player> taxiFantasyTeam;
    TableColumn taxiFtPos;
    TableColumn taxiFtFirstName;
    TableColumn taxiFtLastName;
    TableColumn taxiFtProTeam;
    TableColumn taxiFtPositions;
    TableColumn taxiFtRuns;
    TableColumn taxiFtWalks;
    TableColumn taxiFtHomeruns;
    TableColumn taxiFtSaves;
    TableColumn taxiFtRBI;
    TableColumn taxiFtStrikeout;
    TableColumn taxiFtStolenBases;
    TableColumn taxiFtERA;
    TableColumn taxiFtBattingAvg;
    TableColumn taxiFtWHIP;
    TableColumn taxiFtEstimatedValue;
    TableColumn taxiFtcontract;        
    
    //MISC MLB TEAM PAGE ELEMENTS
    ComboBox mlbTeamsComboBox;
    TableView mlbTeamTable;
    TableColumn mlbPlayerlastName;
    TableColumn mlbPlayerFirstName;
    TableColumn mlbPlayerQualifyingPositions;
    
    //MISC FANTASY STANDING TEAM PAGE ELEMENTS
    TableView fantasyTeamStandingsTable;
    TableColumn teamName;
    TableColumn standingsPlayersNeeded;
    TableColumn standingsMoneyLeft;
    TableColumn standingsAVGMoneyPerPlayer;
    TableColumn standingsAVGRuns;
    TableColumn standingsAVGWalks;
    TableColumn standingsAVGHomeruns;
    TableColumn standingsAVGSaves;
    TableColumn standingsAVGRBI;
    TableColumn standingsAVGStrikeout;
    TableColumn standingsAVGStolenBases;
    TableColumn standingsAVGERA;
    TableColumn standingsAVGBattingAvg;
    TableColumn standingsAVGWHIP;
    TableColumn standingsTotalPoints;
    
    //DRAFT PAGE
    HBox draftButtonHBox;
    Button draftPlayer;
    Button autoDraftStart;
    Button autoDraftPause;
    TableView draftTable;
    TableColumn draftPickNumberColumn;
    TableColumn draftFirstNameColumn;
    TableColumn draftLastNameColumn;
    TableColumn draftFantasyTeamColumn;
    TableColumn draftContractColumn;
    TableColumn draftSalaryColumn;
    
    /**
     * Constructor for making this GUI
     * @param initPrimaryStage 
     */
    public WBDK_GUI(Stage initPrimaryStage) {
        primaryStage = initPrimaryStage;
    } 
    
    public void initGui(String windowTitle) {   
        
        try{
            initDialogs();
            
            initFileToolbar();
            
            initPaneToolbar();
            
            initSortButtons();
            
            initButtons();
            
            createTables();
                                    
            initWindow(windowTitle);
            
            initVBoxScreens();
            
            initEventHandlers();
            
        }catch(IOException e) {
            
        }
        
    }
    
    private Button initChildButton(HBox toolbar, WBDK_PropertyType icon, 
            WBDK_PropertyType tooltip, boolean disabled) {
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_IMAGES + props.getProperty(icon.toString());
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        return button;
    }
    
    public void initDialogs() {
        messageDialog = new MessageDialog(primaryStage, CLOSE_BUTTON_LABEL);
        yesNoCancelDialog = new YesNoCancelDialog(primaryStage);
    }
    
    public void initFileToolbar() {
        
        //CREATE A PLACE TO HOLD THE BUTTONS
        fileToolbar = new HBox();
        
        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOMOE WILL
        // START AS ENABLED (FALSE). WHILE OTHERS DISABLED (TRUE)
        newDraft = initChildButton(fileToolbar, WBDK_PropertyType.NEW_DRAFT_ICON, WBDK_PropertyType.NEW_DRAFT_TOOLTIP, false);
        loadDraft = initChildButton(fileToolbar, WBDK_PropertyType.LOAD_DRAFT_ICON, WBDK_PropertyType.LOAD_DRAFT_TOOLTIP, false);
        saveDraft = initChildButton(fileToolbar, WBDK_PropertyType.SAVE_DRAFT_ICON, WBDK_PropertyType.SAVE_DRAFT_TOOLTIP, true);
        exportDraft = initChildButton(fileToolbar, WBDK_PropertyType.EXPORT_DRAFT_ICON, WBDK_PropertyType.EXPORT_DRAFT_TOOLTIP, true);
        exitDraft = initChildButton(fileToolbar, WBDK_PropertyType.EXIT_DRAFT_ICON, WBDK_PropertyType.EXIT_DRAFT_TOOLTIP, false);
        
    }
    
    public void initPaneToolbar() {
        
        //CREATE A PLACE TO HOLD THE BUTTONS
        paneToolbar = new HBox();
        
        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOMOE WILL
        // START AS ENABLED (FALSE)
        playersButton = initChildButton(paneToolbar, WBDK_PropertyType.PLAYERS_ICON, WBDK_PropertyType.PLAYERS_TOOLTIP, false);
        fantasyTeamsButton = initChildButton(paneToolbar, WBDK_PropertyType.FANTASY_TEAMS_ICON, WBDK_PropertyType.FANTASY_TEAMS_TOOLTIP, false);
        fantasyStandingsButton = initChildButton(paneToolbar, WBDK_PropertyType.FANTASY_STANDINGS_ICON, WBDK_PropertyType.FANTASY_STANDINGS_TOOLTIP, false);
        draftButton = initChildButton(paneToolbar, WBDK_PropertyType.DRAFT_ICON, WBDK_PropertyType.DRAFT_TOOLTIP, false);
        mlbTeamsButton = initChildButton(paneToolbar, WBDK_PropertyType.MLB_TEAMS_ICON, WBDK_PropertyType.MLB_TOOLTIP, false);
        
    }
    
    public void drawFileToolbar() {
        mainPane.setBottom(paneToolbar);      
    }
    
    public void paintMainPane() {
        mainPane.setStyle("-fx-background-color: linear-gradient(to bottom, FF856E, white)");
    }
    
    public void initSortButtons() {
        searchBox = new TextField();
        searchBox.setPrefWidth(200);
        sortHBox = new HBox();
        sortHBox.setSpacing(20);
        all = new RadioButton();
        all.setSelected(true);
        all.setToggleGroup(group);
        C = new RadioButton();
        C.setToggleGroup(group);
        firstBase = new RadioButton();
        firstBase.setToggleGroup(group);
        CL = new RadioButton();
        CL.setToggleGroup(group);
        thirdBase = new RadioButton();
        thirdBase.setToggleGroup(group);
        secondBase = new RadioButton();
        secondBase.setToggleGroup(group);
        MI = new RadioButton();
        MI.setToggleGroup(group);
        SS = new RadioButton();
        SS.setToggleGroup(group);
        OF = new RadioButton();
        OF.setToggleGroup(group);
        U = new RadioButton();
        U.setToggleGroup(group);
        P = new RadioButton();
        P.setToggleGroup(group);
        all.setText("All");
        C.setText("C");
        firstBase.setText("1B");
        CL.setText("CL");
        thirdBase.setText("3B");
        secondBase.setText("2B");
        MI.setText("MI");
        SS.setText("SS");
        OF.setText("OF");
        U.setText("U");
        P.setText("P");
        sortHBox.getChildren().addAll(all, C, firstBase, CL, thirdBase, secondBase, MI,
                SS, OF, U, P);
    }
    
    public void createTables() {
        
        //AVAILABLE PLAYERS TABLE
        firstName = new TableColumn(COL_FIRST);
        lastName = new TableColumn(COL_LAST);
        proTeam = new TableColumn(COL_TEAM);
        positions = new TableColumn(COL_POS);
        dob = new TableColumn(COL_DOB);
        runs =  new TableColumn(COL_RUNS);
        walks = new TableColumn(COL_WALKS);
        homeruns = new TableColumn(COL_HR);
        saves = new TableColumn(COL_SV);
        rbi = new TableColumn(COL_RBI);
        strikeouts = new TableColumn(COL_K);
        stolenBases = new TableColumn(COL_SB);
        era = new TableColumn(COL_ERA);
        battingAvg = new TableColumn(COL_BA);
        whip = new TableColumn(COL_WHIP);
        estimatedValue = new TableColumn(COL_VALUE);
        notes = new TableColumn(COL_NOTES);
        
        
        firstName.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        lastName.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        proTeam.setCellValueFactory(new PropertyValueFactory<String, String>("team"));
        positions.setCellValueFactory(new PropertyValueFactory<String, String>("position"));
        dob.setCellValueFactory(new PropertyValueFactory<Integer, String>("yearOfBirth"));
        runs.setCellValueFactory(new PropertyValueFactory<Integer, String>("runs"));
        walks.setCellValueFactory(new PropertyValueFactory<Integer, String>("walks"));
        homeruns.setCellValueFactory(new PropertyValueFactory<Integer, String>("homeruns"));
        saves.setCellValueFactory(new PropertyValueFactory<Integer, String>("saves"));
        rbi.setCellValueFactory(new PropertyValueFactory<Integer, String>("runsBattedIn"));
        strikeouts.setCellValueFactory(new PropertyValueFactory<Integer, String>("strikeouts"));
        stolenBases.setCellValueFactory(new PropertyValueFactory<Integer, String>("stolenBases"));
        era.setCellValueFactory(new PropertyValueFactory<Double, String>("earnedRunAvg"));
        battingAvg.setCellValueFactory(new PropertyValueFactory<Double, String>("battingAvg"));
        whip.setCellValueFactory(new PropertyValueFactory<Double, String>("walksHitsInningsPitched"));
        estimatedValue.setCellValueFactory(new PropertyValueFactory<Integer, String>("value"));
        notes.setCellValueFactory(new PropertyValueFactory<String, String>("notes"));
        notes.setCellFactory(TextFieldTableCell.forTableColumn());
        
        
        availablePlayers = new TableView();
        availablePlayers.setPrefHeight(1000);
        availablePlayers.getColumns().setAll(firstName, lastName, proTeam, positions, dob, runs, walks, homeruns,
                saves, rbi, strikeouts, stolenBases, era, battingAvg, whip, estimatedValue, notes);
        
        availablePlayers.setItems(dataManager.getPlayers());
        
        //FANTASY TEAMS TABLE 
        //STARTING
        startingFtPos = new TableColumn("Position");
        startingFtFirstName = new TableColumn(COL_FIRST);
        startingFtLastName = new TableColumn(COL_LAST);
        startingFtProTeam = new TableColumn(COL_TEAM);
        startingFtPositions = new TableColumn(COL_POS);
        startingFtRuns = new TableColumn(COL_RUNS);
        startingFtWalks = new TableColumn(COL_WALKS);
        startingFtHomeruns = new TableColumn(COL_HR);
        startingFtSaves = new TableColumn(COL_SV);
        startingFtRBI = new TableColumn(COL_RBI);
        startingFtStrikeout = new TableColumn(COL_K);
        startingFtStolenBases = new TableColumn(COL_SB);
        startingFtERA = new TableColumn(COL_ERA);
        startingFtBattingAvg = new TableColumn(COL_BA);
        startingFtWHIP = new TableColumn(COL_WHIP);
        startingFtEstimatedValue = new TableColumn(COL_VALUE);
        startingFtcontract = new TableColumn("Contract");        
        
        startingFtPos.setCellValueFactory(new PropertyValueFactory<String, String>("chosenPosition"));
        startingFtFirstName.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        startingFtLastName.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        startingFtProTeam.setCellValueFactory(new PropertyValueFactory<Team, String>("team"));
        startingFtPositions.setCellValueFactory(new PropertyValueFactory<String, String>("position"));
        startingFtRuns.setCellValueFactory(new PropertyValueFactory<Integer, String>("runs"));
        startingFtWalks.setCellValueFactory(new PropertyValueFactory<Integer, String>("walks"));
        startingFtHomeruns.setCellValueFactory(new PropertyValueFactory<Integer, String>("homeruns"));
        startingFtSaves.setCellValueFactory(new PropertyValueFactory<Integer, String>("saves"));
        startingFtRBI.setCellValueFactory(new PropertyValueFactory<Integer, String>("runsBattedIn"));
        startingFtStrikeout.setCellValueFactory(new PropertyValueFactory<Integer, String>("strikeouts"));
        startingFtStolenBases.setCellValueFactory(new PropertyValueFactory<Integer, String>("stolenBases"));
        startingFtERA.setCellValueFactory(new PropertyValueFactory<Double, String>("earnedRunAvg"));
        startingFtBattingAvg.setCellValueFactory(new PropertyValueFactory<Double, String>("battingAvg"));
        startingFtWHIP.setCellValueFactory(new PropertyValueFactory<Double, String>("walksHitsInningsPitched"));
        startingFtEstimatedValue.setCellValueFactory(new PropertyValueFactory<Integer, String>("value"));
        startingFtcontract.setCellValueFactory(new PropertyValueFactory<String, String>("contract"));
        
        //TAXI
        taxiFtPos = new TableColumn("Position");
        taxiFtFirstName = new TableColumn(COL_FIRST);
        taxiFtLastName = new TableColumn(COL_LAST);
        taxiFtProTeam = new TableColumn(COL_TEAM);
        taxiFtPositions = new TableColumn(COL_POS);
        taxiFtRuns = new TableColumn(COL_RUNS);
        taxiFtWalks = new TableColumn(COL_WALKS);
        taxiFtHomeruns = new TableColumn(COL_HR);
        taxiFtSaves = new TableColumn(COL_SV);
        taxiFtRBI = new TableColumn(COL_RBI);
        taxiFtStrikeout = new TableColumn(COL_K);
        taxiFtStolenBases = new TableColumn(COL_SB);
        taxiFtERA = new TableColumn(COL_ERA);
        taxiFtBattingAvg = new TableColumn(COL_BA);
        taxiFtWHIP = new TableColumn(COL_WHIP);
        taxiFtEstimatedValue = new TableColumn(COL_VALUE);
        taxiFtcontract = new TableColumn("Contract");             
        
        taxiFtPos.setCellValueFactory(new PropertyValueFactory<String, String>("chosenPosition"));
        taxiFtFirstName.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        taxiFtLastName.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        taxiFtProTeam.setCellValueFactory(new PropertyValueFactory<Team, String>("team"));
        taxiFtPositions.setCellValueFactory(new PropertyValueFactory<String, String>("position"));
        taxiFtRuns.setCellValueFactory(new PropertyValueFactory<Integer, String>("runs"));
        taxiFtWalks.setCellValueFactory(new PropertyValueFactory<Integer, String>("walks"));
        taxiFtHomeruns.setCellValueFactory(new PropertyValueFactory<Integer, String>("homeruns"));
        taxiFtSaves.setCellValueFactory(new PropertyValueFactory<Integer, String>("saves"));
        taxiFtRBI.setCellValueFactory(new PropertyValueFactory<>("runsBattedIn"));
        taxiFtStrikeout.setCellValueFactory(new PropertyValueFactory<Integer, String>("strikeouts"));
        taxiFtStolenBases.setCellValueFactory(new PropertyValueFactory<Integer, String>("stolenBases"));
        taxiFtERA.setCellValueFactory(new PropertyValueFactory<Double, String>("earnedRunAvg"));
        taxiFtBattingAvg.setCellValueFactory(new PropertyValueFactory<Double, String>("battingAvg"));
        taxiFtWHIP.setCellValueFactory(new PropertyValueFactory<Double, String>("walksHitsInningsPitched"));
        taxiFtEstimatedValue.setCellValueFactory(new PropertyValueFactory<Integer, String>("value"));
        taxiFtcontract.setCellValueFactory(new PropertyValueFactory<String, String>("contract"));
        
        startingFantasyTeam = new TableView();
        startingFantasyTeam.setPrefHeight(1000);
        startingFantasyTeam.getColumns().setAll(startingFtPos, startingFtFirstName, startingFtLastName,
                startingFtProTeam, startingFtPositions, startingFtRuns, startingFtWalks, startingFtHomeruns, 
                startingFtSaves, startingFtRBI, startingFtStrikeout, startingFtStolenBases, startingFtERA,
                startingFtBattingAvg, startingFtWHIP, startingFtEstimatedValue, startingFtcontract);

        
        taxiFantasyTeam = new TableView();
        taxiFantasyTeam.setPrefHeight(1000);
        taxiFantasyTeam.getColumns().setAll(taxiFtPos, taxiFtFirstName, taxiFtLastName, taxiFtProTeam,
                taxiFtPositions, taxiFtRuns, taxiFtWalks, taxiFtHomeruns,
                taxiFtSaves, taxiFtRBI, taxiFtStrikeout, taxiFtStolenBases, taxiFtERA,
                taxiFtBattingAvg, taxiFtWHIP, taxiFtEstimatedValue, taxiFtcontract);
        
        //MLB TEAMS
        mlbPlayerlastName = new TableColumn(COL_LAST);
        mlbPlayerFirstName = new TableColumn(COL_FIRST);
        mlbPlayerQualifyingPositions = new TableColumn(COL_POS);
        
        mlbPlayerlastName.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        mlbPlayerFirstName.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        mlbPlayerQualifyingPositions.setCellValueFactory(new PropertyValueFactory<String, String>("position"));
        
        mlbPlayerlastName.setPrefWidth(150);
        mlbPlayerFirstName.setPrefWidth(150);
        mlbPlayerQualifyingPositions.setPrefWidth(150);
        
        mlbTeamTable = new TableView();
        mlbTeamTable.setPrefHeight(1000);
        mlbTeamTable.getColumns().setAll(mlbPlayerlastName, mlbPlayerFirstName, mlbPlayerQualifyingPositions);
        
        //FANTASY STANDINGS
        teamName = new TableColumn("Team Name");
        standingsPlayersNeeded = new TableColumn("Players Needed");
        standingsMoneyLeft = new TableColumn("$ Left");
        standingsAVGMoneyPerPlayer = new TableColumn("$ PP");
        standingsAVGRuns = new TableColumn(COL_RUNS);
        standingsAVGWalks = new TableColumn(COL_WALKS);
        standingsAVGHomeruns = new TableColumn(COL_HR);
        standingsAVGSaves = new TableColumn(COL_SV);
        standingsAVGRBI = new TableColumn(COL_RBI);
        standingsAVGStrikeout = new TableColumn(COL_K);
        standingsAVGStolenBases = new TableColumn(COL_SB);
        standingsAVGERA = new TableColumn(COL_ERA);
        standingsAVGBattingAvg = new TableColumn(COL_BA);
        standingsAVGWHIP = new TableColumn(COL_WHIP);
        standingsTotalPoints = new TableColumn("Total Points");
        
        teamName.setCellValueFactory(new PropertyValueFactory<String, String>("name"));
        standingsPlayersNeeded.setCellValueFactory(new PropertyValueFactory<Integer, String>("playersNeeded"));
        standingsMoneyLeft.setCellValueFactory(new PropertyValueFactory<Integer, String>("moneyLeft"));
        standingsAVGMoneyPerPlayer.setCellValueFactory(new PropertyValueFactory<Integer, String>("moneyPerPlayer"));
        standingsAVGRuns.setCellValueFactory(new PropertyValueFactory<Integer, String>("runs"));
        standingsAVGWalks.setCellValueFactory(new PropertyValueFactory<Integer, String>("walks"));
        standingsAVGHomeruns.setCellValueFactory(new PropertyValueFactory<Integer, String>("homeruns"));
        standingsAVGSaves.setCellValueFactory(new PropertyValueFactory<Integer, String>("saves"));
        standingsAVGRBI.setCellValueFactory(new PropertyValueFactory<Integer, String>("runsBattedIn"));
        standingsAVGStrikeout.setCellValueFactory(new PropertyValueFactory<Integer, String>("strikeouts"));
        standingsAVGStolenBases.setCellValueFactory(new PropertyValueFactory<Integer, String>("stolenBases"));
        standingsAVGERA.setCellValueFactory(new PropertyValueFactory<Double, String>("earnedRunAvg"));
        standingsAVGBattingAvg.setCellValueFactory(new PropertyValueFactory<Double, String>("battingAvg"));
        standingsAVGWHIP.setCellValueFactory(new PropertyValueFactory<Double, String>("walksHitsInningsPitched"));        
        standingsTotalPoints.setCellValueFactory(new PropertyValueFactory<Integer, String>("totalPoints"));
        
        fantasyTeamStandingsTable = new TableView();
        fantasyTeamStandingsTable.setPrefHeight(1000);
        fantasyTeamStandingsTable.getColumns().addAll(teamName, standingsPlayersNeeded,
                standingsMoneyLeft, standingsAVGMoneyPerPlayer, standingsAVGRuns,
                standingsAVGHomeruns, standingsAVGRBI, standingsAVGStolenBases,
                standingsAVGBattingAvg, standingsAVGWalks, standingsAVGSaves,
                standingsAVGStrikeout, standingsAVGERA, standingsAVGWHIP,
                standingsTotalPoints);        
        
        //DRAFT TABLE
        draftPickNumberColumn = new TableColumn("Pick #");
        draftFirstNameColumn = new TableColumn(COL_FIRST);
        draftLastNameColumn = new TableColumn(COL_LAST);
        draftFantasyTeamColumn = new TableColumn("Team");
        draftContractColumn = new TableColumn("Contract");
        draftSalaryColumn = new TableColumn("Salary ($)");
        
        draftTable = new TableView();
        draftTable.setItems(dataManager.getDraftedPlayers());
        draftPickNumberColumn.setCellValueFactory(new PropertyValueFactory<Integer, String>("draftNumber"));
        draftFirstNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        draftLastNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        draftFantasyTeamColumn.setCellValueFactory(new PropertyValueFactory<String, String>("teamDraftedTo"));
        draftContractColumn.setCellValueFactory(new PropertyValueFactory<String, String>("contract"));
        draftSalaryColumn.setCellValueFactory(new PropertyValueFactory<Double, String>("salary"));
        
        draftPickNumberColumn.setPrefWidth(100);
        draftFirstNameColumn.setPrefWidth(100);
        draftLastNameColumn.setPrefWidth(100);
        draftFantasyTeamColumn.setPrefWidth(100);
        draftContractColumn.setPrefWidth(100);
        draftSalaryColumn.setPrefWidth(100);
        
        draftTable.setPrefHeight(1000);
        draftTable.getColumns().addAll(draftPickNumberColumn, draftFirstNameColumn, draftLastNameColumn,
                draftFantasyTeamColumn, draftContractColumn, draftSalaryColumn);
    }
    
    public void initButtons() {
        buttonsHBox = new HBox();
        fantasyTeamsButtonsHBox = new HBox();
        draftButtonHBox = new HBox();
        
        addPlayer =  initChildButton(buttonsHBox, WBDK_PropertyType.ADD_ICON, WBDK_PropertyType.ADD_PLAYER_TOOLTIP, false);    
        removePlayer = initChildButton(buttonsHBox, WBDK_PropertyType.MINUS_ICON, WBDK_PropertyType.REMOVE_PLAYER_TOOLTIP, false);
        
        addFantasyTeam = initChildButton(fantasyTeamsButtonsHBox, WBDK_PropertyType.ADD_ICON, WBDK_PropertyType.ADD_FANTASY_TEAM_TOOLTIP, false);
        removeFantasyTeam = initChildButton(fantasyTeamsButtonsHBox, WBDK_PropertyType.MINUS_ICON, WBDK_PropertyType.REMOVE_FANTASY_TEAM_TOOLTIP, false);
        editFantasyTeam = initChildButton(fantasyTeamsButtonsHBox, WBDK_PropertyType.EDIT_ICON, WBDK_PropertyType.EDIT_FANTASY_TEAM_TOOLTIP, false);
        
        draftPlayer = initChildButton(draftButtonHBox, WBDK_PropertyType.RANDOM_ICON, WBDK_PropertyType.DRAFT_PLAYER_TOOLTIP, false);
        autoDraftStart = initChildButton(draftButtonHBox, WBDK_PropertyType.START_ICON, WBDK_PropertyType.AUTO_DRAFT_START_TOOLTIP, false);
        autoDraftPause = initChildButton(draftButtonHBox, WBDK_PropertyType.PAUSE_ICON, WBDK_PropertyType.AUTO_DRAFT_PAUSE_TOOLTIP, false);
    }
    
    public void initVBoxScreens() {
        Label players = new Label("Players");
        Label fantasyTeams = new Label("Fantasy Teams");
        Label fantasyStandings = new Label("Fantasy Standings");
        Label draft = new Label("Draft");
        Label mlbTeams = new Label("MLB Teams");
        
        Font font = new Font("Arial Black", 30);
        
        //Here is where we will build our pages
        //PLAYERS
        playersPane = new VBox();
        playersPane.setStyle("-fx-background-color: white");
        playersPane.setMaxSize(screen.getBounds().getWidth()-200, screen.getBounds().getHeight()-200);
        players.setFont(font);

        searchLabel = new Label("Search:");
        
        //ADD EVERYTHING TO THE PAGE
        buttonsHBox.getChildren().add(searchLabel);
        buttonsHBox.getChildren().add(searchBox);
        playersPane.getChildren().add(players);
        playersPane.getChildren().add(buttonsHBox);
        playersPane.getChildren().add(sortHBox);
        playersPane.getChildren().add(availablePlayers);
        
        
        //FANTASY TEAMS
        fantasyTeamsPane = new VBox();
        fantasyTeamsPane.setStyle("-fx-background-color: white");
        fantasyTeamsPane.setMaxSize(screen.getBounds().getWidth()-200, screen.getBounds().getHeight()-200);
        fantasyTeams.setFont(font);
        selectFantasyTeamLabel = new Label("Select Fantasy Team: ");
        fantasyTeamsComboBox = new ComboBox();
        fantasyTeamsButtonsHBox.getChildren().addAll(selectFantasyTeamLabel, fantasyTeamsComboBox);
        
        startingVBox = new VBox();
        startingVBox.setPadding(new Insets(20));
        startingLineupLabel = new Label("Starting Lineup");
        startingVBox.getChildren().addAll(startingLineupLabel, startingFantasyTeam);        
        
        taxiVBox = new VBox();
        taxiVBox.setPadding(new Insets(20));
        taxiSquadLabel = new Label("Taxi Squad");
        taxiVBox.getChildren().addAll(taxiSquadLabel, taxiFantasyTeam);
        
        //ADD EVERYTHING TO THE PAGE
        fantasyTeamsPane.getChildren().add(fantasyTeams);
        fantasyTeamsPane.getChildren().add(fantasyTeamsButtonsHBox);
        fantasyTeamsPane.getChildren().add(startingVBox);
        fantasyTeamsPane.getChildren().add(taxiVBox);
        
        //FANTASY STANDINGS
        fantasyStandingsPane = new VBox();
        fantasyStandingsPane.setStyle("-fx-background-color: white");
        fantasyStandingsPane.setMaxSize(screen.getBounds().getWidth()-200, screen.getBounds().getHeight()-200);
        fantasyStandings.setFont(font);
        fantasyStandingsPane.getChildren().add(fantasyStandings);
        fantasyStandingsPane.getChildren().add(fantasyTeamStandingsTable);
        
        //DRAFT
        draftPane = new VBox();
        draftPane.setStyle("-fx-background-color: white");
        draftPane.setMaxSize(screen.getBounds().getWidth()-200, screen.getBounds().getHeight()-200);
        draft.setFont(font);
        draftPane.getChildren().add(draft); 
        draftPane.getChildren().add(draftButtonHBox);
        draftPane.getChildren().add(draftTable);
       
        //MLBtEAMS
        mlbTeamsPane = new VBox();
        mlbTeamsPane.setStyle("-fx-background-color: white");
        mlbTeamsPane.setMaxSize(screen.getBounds().getWidth()-200, screen.getBounds().getHeight()-200);
        mlbTeams.setFont(font);
        mlbTeamsComboBox = new ComboBox();
        mlbTeamsComboBox.getItems().setAll("ATL", "AZ", "CHC", "CIN", "COL", "LAD",
                "MIA", "MIL", "NYM", "PHI", "PIT", "SD", "SF", "STL", "WAS");
        mlbTeamsPane.getChildren().add(mlbTeams);
        mlbTeamsPane.getChildren().add(mlbTeamsComboBox);
        mlbTeamsPane.getChildren().add(mlbTeamTable);
        
        
    }
    
    /**
     * This method created the main window for the entire program
     * By Default, it will start with just a blank screen file toolbar
     * @param windowTitle 
     */
    public void initWindow(String windowTitle) {
        
        //SET THE WINDOW TITLE
        primaryStage.setTitle(windowTitle);
        
        //GET SIZE OF THE SCREEN
        screen = Screen.getPrimary();
        
        //USE SCREEN SIZE TO SET SIZE OF WINDOW
        primaryStage.setX(screen.getBounds().getMinX());
        primaryStage.setY(screen.getBounds().getMinY());
        primaryStage.setWidth(screen.getBounds().getWidth());
        primaryStage.setHeight(screen.getBounds().getHeight());
        
        //ONLY ADD THE FILE TOOLBAR.  THAT IS THE MAIN SCREEN THAT 
        //NEEDS TO BE SHOWN FIRST.
        mainPane = new BorderPane();
        mainPane.setTop(fileToolbar);
        mainScene = new Scene(mainPane);
        
        //TIE SCENE TO THE WINDOW
        primaryStage.setScene(mainScene);
        primaryStage.show();
        
    }
    
    //INIT ALL THE EVENT HANDLERS
    public void initEventHandlers() throws IOException {
        fileController = new FileController(messageDialog, yesNoCancelDialog);
        screenController = new ScreenController();
        //FILE CONTROLS
        newDraft.setOnAction(e -> {
            fileController.handleNewDraftRequest(this);
            dataManager = DataManager.getDataManager();
            dataManager.loadHitters(JSON_FILE_PATH_HITTERS, "Hitters");
            dataManager.loadPitchers(JSON_FILE_PATH_PITCHERS, "Pitchers");
            dataManager.putMLBPlayerIntoMap();
            dataManager.putPlayersIntoMap(dataManager.getPlayers());
            screenController.handleFantasyTeamsScreenRequest(this);
        });
        loadDraft.setOnAction(e -> {
            fileController.handleLoadDraftRequest(this);
        });
        saveDraft.setOnAction(e -> {
            fileController.handleSaveRequest(this);
        });
        exportDraft.setOnAction(e -> {
            fileController.handleExportDraftRequest(this);
        });
        exitDraft.setOnAction(e -> {
            fileController.handleExitDraftRequest(this);
        });
        
        //SCREEN CONTROLS
        playersButton.setOnAction(e -> {
            screenController.handlePlayerScreenRequest(this);
        });
        fantasyTeamsButton.setOnAction(e -> {
            screenController.handleFantasyTeamsScreenRequest(this);
        });
        fantasyStandingsButton.setOnAction(e -> {
            screenController.handleFantasyStandingsScreenRequest(this);
        });
        draftButton.setOnAction(e -> {
            screenController.handleDraftScreenRequest(this);
        });
        mlbTeamsButton.setOnAction(e -> {
            screenController.handleMLBTeamsScreenRequest(this);
        });
        
        //PLAYERS BUTTONS
        addPlayer.setOnAction(e -> {
            Player temp = new Hitter();
            playerController = new PlayerController(primaryStage, temp, messageDialog, yesNoCancelDialog);
            playerController.handlePlayerAddController();
        });
        
        removePlayer.setOnAction(e -> {
            Player temp = availablePlayers.getSelectionModel().getSelectedItem();
            playerController = new PlayerController(primaryStage, temp, messageDialog, yesNoCancelDialog);
            playerController.handlePlayerRemoveController(temp);
        });
        
        //FANTASY TEAM BUTTONS
        addFantasyTeam.setOnAction(e -> {
            FantasyTeam temp = null;
            fantasyTeamsController = new FantasyTeamController(primaryStage, messageDialog, yesNoCancelDialog, temp);            
            fantasyTeamsController.handleAddFantasyTeamRequest();
            populateFantasyTeamsComboBox();
            fantasyTeamStandingsTable.setItems(dataManager.getFantasyTeams());
            //fantasyTeamsComboBox.getSelectionModel().selectLast();                                
        });
        removeFantasyTeam.setOnAction(e -> {
            String teamToRemove = (String)fantasyTeamsComboBox.getSelectionModel().getSelectedItem();
            int indexOfTeam = fantasyTeamsComboBox.getSelectionModel().getSelectedIndex();
            fantasyTeamsController.handleRemoveFantasyTeamRequest(teamToRemove, indexOfTeam);
            populateFantasyTeamsComboBox();
            fantasyTeamStandingsTable.setItems(dataManager.getFantasyTeams());            
            //IF THE TEAM BEING REMOVED ISN'T THE LAST
            if(fantasyTeamsComboBox.getItems().size() != 0) {
                fantasyTeamsComboBox.getSelectionModel().selectLast();   
            }
            else {
                startingFantasyTeam.setItems(null);
            }
        });
        editFantasyTeam.setOnAction(e -> {
            FantasyTeam teamToEdit = (FantasyTeam)dataManager.getFantasyTeams().get(fantasyTeamsComboBox.getSelectionModel().getSelectedIndex());
            fantasyTeamsController = new FantasyTeamController(primaryStage, messageDialog, yesNoCancelDialog, teamToEdit);            
            fantasyTeamsController.handleEditFantasyTeamRequest(teamToEdit);
            int currentSelection = fantasyTeamsComboBox.getSelectionModel().getSelectedIndex();
            populateFantasyTeamsComboBox();                   
            fantasyTeamsComboBox.getSelectionModel().select(currentSelection);
        });
        
        //RADIO BUTTONS FOR SORTING
        sortController = new SortController();
        all.setOnAction(e -> {
            sortController.handleAllSort(availablePlayers);
        });
        C.setOnAction(e -> {
            sortController.handleCSort(availablePlayers);
        });
        firstBase.setOnAction(e -> {
            sortController.handle1BSort(availablePlayers);
        });
        CL.setOnAction(e -> {
            sortController.handleClSort(availablePlayers);
        });
        thirdBase.setOnAction(e -> {
            sortController.handle3BSort(availablePlayers);
        });
        secondBase.setOnAction(e -> {
            sortController.handle2BSort(availablePlayers);
        });
        MI.setOnAction(e -> {
            sortController.handleMlSort(availablePlayers);
        });
        SS.setOnAction(e -> {
            sortController.handleSSSort(availablePlayers);
        });
        OF.setOnAction(e -> {
            sortController.handleOFSort(availablePlayers);
        });
        U.setOnAction(e -> {
            sortController.handleUSort(availablePlayers);
        });
        P.setOnAction(e -> {
            sortController.handlePSort(availablePlayers);
        });
        
        //MAKE NOTES EDITTABLE

        availablePlayers.setOnMouseClicked(e -> {
            if(e.getClickCount() == 2) {
                //OPEN THE EDITOR
                Player temp = availablePlayers.getSelectionModel().getSelectedItem();
                playerController = new PlayerController(primaryStage, temp, messageDialog, yesNoCancelDialog);
                playerController.handlePlayerEditController(availablePlayers, temp, availablePlayers);
                TableColumn update = (TableColumn)fantasyTeamStandingsTable.getColumns().get(0);
                update.setVisible(false);
                update.setVisible(true);
            }
        });
                
        startingFantasyTeam.setOnMouseClicked(e -> {
            if(e.getClickCount() == 2) {
                //OPEN THE EDITOR
                Player temp = startingFantasyTeam.getSelectionModel().getSelectedItem();
                playerController = new PlayerController(primaryStage, temp, messageDialog, yesNoCancelDialog);
                playerController.handlePlayerEditController(startingFantasyTeam, temp, availablePlayers);
                TableColumn update = (TableColumn)fantasyTeamStandingsTable.getColumns().get(0);
                update.setVisible(false);
                update.setVisible(true);                
            }
        });
        
        searchBox.textProperty().addListener((observable, oldValue, newValue) -> {
            sortController.handleSearchRequest(availablePlayers, newValue);
        });

        fantasyTeamsComboBox.setOnAction(e -> {
            int index = fantasyTeamsComboBox.getSelectionModel().getSelectedIndex();
            FantasyTeam team = (FantasyTeam)dataManager.getFantasyTeams().get(index);            
            startingFantasyTeam.setItems(team.getStartingPlayer());
            //startingFantasyTeam.setItems(team.getTaxiPlayes());
        });
        
        mlbTeamsComboBox.setOnAction(e -> {
            ObservableMap<String, ObservableList<Player>> mlbTeamsMap = dataManager.getMLBTeams();
            String key = (String)mlbTeamsComboBox.getSelectionModel().getSelectedItem();
            ObservableList<Player> mlbTeam = mlbTeamsMap.get(key);
            mlbTeamTable.setItems(mlbTeam);
        });
        
        //DRAFT BUTTONS
        draftPlayer.setOnAction(e -> {
            draftController = new DraftController();
            draftController.draftPlayer();
            draftController.updateDraft();
        });
        
    }
    
    public Stage getPrimaryStage() {
        return this.primaryStage;
    }
    
    public BorderPane getMainPane() {
        return this.mainPane;
    }
    
    public VBox getPlayersVBox() {
        return this.playersPane;
    }
    
    public VBox getFantasyTeamsVBox() {
        return this.fantasyTeamsPane;
    }
    
    public VBox getFantasyStandingsVBox() {
        return this.fantasyStandingsPane;
    }
    
    public VBox getDraftVBox() {
        return this.draftPane;
    }
    
    public VBox getMLBTeamsVBox() {
        return this.mlbTeamsPane;
    } 
    
    public void populateFantasyTeamsComboBox() {
        ObservableList<FantasyTeam> fantasyTeams = FXCollections.observableArrayList();
        fantasyTeams.setAll(dataManager.getFantasyTeams());
        ObservableList<String> fantasyTeamNames = FXCollections.observableArrayList();
        for(FantasyTeam e : fantasyTeams) {
            fantasyTeamNames.add(e.getName());
        }
        fantasyTeamsComboBox.setItems(fantasyTeamNames);
    }
    
}
