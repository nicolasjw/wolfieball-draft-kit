package wbdk.gui;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import wbdk.data.DataManager;
import wbdk.data.FantasyTeam;
import wbdk.data.Player;

/**
 *
 * @author McKillaGorilla
 */
public class PlayerItemDialog  extends Stage {
    // THIS IS THE OBJECT DATA BEHIND THIS UI
    Player newplayer;
    boolean fantasy = false;
    boolean poss = false;
    boolean cont = false;
    boolean salary = false;
    
    DataManager dataManager = DataManager.getDataManager();
    
    // GUI CONTROLS FOR OUR DIALOG
    GridPane gridPane;
    Scene dialogScene;
    Label headingLabel;
    Label fullNameLabel;
    Label positionsLabel;
    Label fantasyTeamLabel;
    Label posForChoosingLabel;
    Label contractLabel;
    Label salaryLabel;
    ComboBox fantasyTeamComboBox;
    ComboBox positionsComboBox;
    ComboBox contractComboBox;
    TextField salaryTextField;
    Button completeButton;
    Button cancelButton;
    ImageView selfie;
    ImageView flag;
    HBox playerInfo;
    VBox playerStuff;
    
    // THIS IS FOR KEEPING TRACK OF WHICH BUTTON THE USER PRESSED
    String selection;
    
    // CONSTANTS FOR OUR UI
    public static final String COMPLETE = "Complete";
    public static final String CANCEL = "Cancel";
    public static final String FANTASY_TEAM_PROMPT = "Fantasy Team: ";
    public static final String POSITION_PROMPT = "Position: ";
    public static final String CONTRACT_PROMPT = "Contract: ";
    public static final String SALARY_PROMPT = "Salary ($): ";    
    public static final String PLAYER_HEADING = "Player Details";
    public static final String EDIT_PLAYER_TITLE = "Edit Player";
    /**
     * Initializes this dialog so that it can be used for either adding
     * new schedule items or editing existing ones.
     * 
     * @param primaryStage The owner of this modal dialog.
     */
    public PlayerItemDialog(Stage primaryStage, Player player,  MessageDialog messageDialog) {       
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        // FIRST OUR CONTAINER
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        // PUT THE HEADING IN THE GRID, NOTE THAT THE TEXT WILL DEPEND
        // ON WHETHER WE'RE ADDING OR EDITING
        headingLabel = new Label(PLAYER_HEADING);
    
        // NOW THE NOTES
        newplayer = player;
        
        playerInfo = new HBox();
        playerStuff = new VBox();
        
        selfie = new ImageView(newplayer.getPhoto());
        flag = new ImageView(newplayer.getFlag());
                

        fullNameLabel = new Label(newplayer.getFirstName() + " " + newplayer.getLastName());
        positionsLabel = new Label(newplayer.getPosition());
        fantasyTeamLabel = new Label(FANTASY_TEAM_PROMPT);
        posForChoosingLabel = new Label(POSITION_PROMPT);
        contractLabel = new Label(CONTRACT_PROMPT);
        salaryLabel = new Label(SALARY_PROMPT);
        
        fantasyTeamComboBox = new ComboBox();
        positionsComboBox = new ComboBox();
        contractComboBox = new ComboBox();
        salaryTextField = new TextField();
        
        playerStuff.getChildren().addAll(flag, fullNameLabel, positionsLabel);
        playerStuff.setSpacing(20);
        
        playerInfo.getChildren().add(selfie);        
        
        
        //FILL COMBO BOXES'
        //FANTASY TEAMS TO CHOOSE FROM
        ObservableList<FantasyTeam> fantasyTeams = FXCollections.observableArrayList();
        fantasyTeams.setAll(dataManager.getFantasyTeams());
        
        ObservableList<String> fantasyTeamNames = FXCollections.observableArrayList();
        for(FantasyTeam e : fantasyTeams) {
            //IF THIS PLAYER IS ON A FANTASY TEAM, DON'T PROVIDE THAT TEAM IN THE BOX
            if(!e.getStartingPlayer().contains(newplayer)) {
                fantasyTeamNames.add(e.getName());
            } 
        }
        if(!dataManager.getPlayers().contains(newplayer)) {
            fantasyTeamNames.add("Free Agents");
        }
        fantasyTeamComboBox.setItems(fantasyTeamNames);
        
        //POSITIONS
        String pos = newplayer.getPosition();
        String[] splitPos = pos.split("_");
        ObservableList<String> playablePos = FXCollections.observableArrayList();
        for(String p : splitPos) {
            playablePos.add(p);
        }
        //CHECK TO SEE WHICH POSITIONS ARE OPEN FOR THIS PLAYER TO PLAY
        
        //CONTRACT
        ObservableList<String> contracts = FXCollections.observableArrayList();
        contracts.addAll("S1", "S2", "X");
        contractComboBox.setItems(contracts);
        
        // AND FINALLY, THE BUTTONS
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);
        // REGISTER EVENT HANDLERS FOR OUR BUTTONS
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            PlayerItemDialog.this.selection = sourceButton.getText();
            if(this.selection.equals("Cancel")) {
                PlayerItemDialog.this.hide();
            }
            else if(this.selection.equals("Complete") && fantasy == true && poss == true
                    && cont == true && salary == true) {
                PlayerItemDialog.this.hide();
            }
        };
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);
        
        fantasyTeamComboBox.setOnAction(e -> {
            //THIS WILL LOAD AVAILABLE POSITIONS THAT THIS PLAYER CAN PLAY
            boolean found = false;
            String selectedTeamName = fantasyTeamComboBox.getSelectionModel().getSelectedItem().toString();
            for(int i = 0; i < dataManager.getFantasyTeams().size(); i++) {
                FantasyTeam temp = (FantasyTeam)dataManager.getFantasyTeams().get(i);
                if(temp.getName().equals(selectedTeamName)) {
                    for(int j = 0; j < playablePos.size(); j++) {
                        for(int k = 0; k < temp.getOpenPositions().length; k++) {
                            //IF THE POSITION ISN'T AVAILABLE, REMOVE IT FROM THE LIST
                            if(temp.getOpenPositions()[k].equals(playablePos.get(j))) {
                                //IF IT IS OPEN, LEAVE IT THERE AND STOP LOOKING
                                found = true;
                                break;
                            }
                        }
                        if(found == false) {
                            //POSITION IS NOT AVAILABLE SO REMOVE IT
                            playablePos.remove(j);
                        }
                    }
                }
            }
            positionsComboBox.setDisable(false);            
            positionsComboBox.setItems(playablePos);            
            fantasy = true;
        });
        positionsComboBox.setOnAction(e -> {
            contractComboBox.setDisable(false);
            String chosenPos = positionsComboBox.getSelectionModel().getSelectedItem().toString();
            newplayer.setChosePosition(chosenPos);
            poss = true;
        });
        contractComboBox.setOnAction(e -> {
            String contract = contractComboBox.getSelectionModel().getSelectedItem().toString();
            newplayer.setContract(contract);
            cont = true;
        });        
        salaryTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            newplayer.setSalary(Double.valueOf(newValue));
            salary = true;
        });
        
        // NOW LET'S ARRANGE THEM ALL AT ONCE
        gridPane.add(headingLabel, 0, 0, 2, 1);
        gridPane.add(playerInfo, 0, 1, 1, 1);
        gridPane.add(playerStuff, 1, 1, 1, 1);
        gridPane.add(fantasyTeamLabel, 0, 2, 1, 1);
        gridPane.add(fantasyTeamComboBox, 1, 2, 1, 1);
        gridPane.add(posForChoosingLabel, 0, 3, 1, 1);
        gridPane.add(positionsComboBox, 1, 3, 1, 1);
        gridPane.add(contractLabel, 0, 4, 1, 1);
        gridPane.add(contractComboBox, 1, 4, 1, 1);
        gridPane.add(salaryLabel, 0, 5, 1, 1);
        gridPane.add(salaryTextField, 1, 5, 1, 1);
        gridPane.add(completeButton, 0, 6, 1, 1);
        gridPane.add(cancelButton, 1, 6, 1, 1);

        positionsComboBox.setDisable(true);
        contractComboBox.setDisable(true);
        
        // AND PUT THE GRID PANE IN THE WINDOW
        dialogScene = new Scene(gridPane);
        this.setScene(dialogScene);
    }
    
    /**
     * Accessor method for getting the selection the user made.
     * 
     * @return Either YES, NO, or CANCEL, depending on which
     * button the user selected when this dialog was presented.
     */
    public String getSelection() {
        return selection;
    }
    
    
    public boolean wasCompleteSelected() {
        return selection.equals(COMPLETE);
    }
    
    public void showEditScheduleItemDialog(Player playerToEdit) {
        // SET THE DIALOG TITLE
        setTitle(EDIT_PLAYER_TITLE);
        
        // LOAD THE SCHEDULE ITEM INTO OUR LOCAL OBJECT
        newplayer.setNotes(playerToEdit.getNotes());
        
        // AND OPEN IT UP
        this.showAndWait();
    }
    
    public Player getPlayer() {
        return this.newplayer;
    }
    
    public String getFantasyTeamName() {
        return this.fantasyTeamComboBox.getSelectionModel().getSelectedItem().toString();
    }
}