package wbdk.gui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import wbdk.data.Player;

/**
 *
 * @author McKillaGorilla
 */
public class AddPlayerDialog extends Stage {
    // THIS IS THE OBJECT DATA BEHIND THIS UI
    Player newplayer;
    
    // GUI CONTROLS FOR OUR DIALOG
    GridPane gridPane;
    Scene dialogScene;
    Label headingLabel;
    Label firstNameLabel;
    TextField firstNameTextField;
    Label lastNameLabel;
    TextField lastNameTextField;;
    Label proTeamLabel;
    ComboBox proTeamComboBox;
    HBox checkBoxHBox;
    CheckBox catcher;
    CheckBox firstBase;
    CheckBox secondBase;
    CheckBox thirdBase;
    CheckBox shortStop;
    CheckBox outField;
    CheckBox pitcher;
    Button completeButton;
    Button cancelButton;
    
    // THIS IS FOR KEEPING TRACK OF WHICH BUTTON THE USER PRESSED
    String selection;
    
    // CONSTANTS FOR OUR UI
    public static final String COMPLETE = "Complete";
    public static final String CANCEL = "Cancel";
    public static final String PLAYER_HEADING = "Player Details";
    public static final String ADD_Player_TITLE = "Add New Player";
    public static final String FIRST_NAME = "First Name: ";
    public static final String LAST_NAME = "Last Name: ";
    public static final String PRO_TEAM = "Pro Team: ";
    /**
     * Initializes this dialog so that it can be used for either adding
     * new schedule items or editing existing ones.
     * 
     * @param primaryStage The owner of this modal dialog.
     */
    public AddPlayerDialog(Stage primaryStage, MessageDialog messageDialog) {       
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        // FIRST OUR CONTAINER
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        // PUT THE HEADING IN THE GRID, NOTE THAT THE TEXT WILL DEPEND
        // ON WHETHER WE'RE ADDING OR EDITING
        headingLabel = new Label(PLAYER_HEADING);
    
        // NOW THE CRITERIA
        firstNameLabel = new Label(FIRST_NAME);
        lastNameLabel = new Label(LAST_NAME);
        proTeamLabel = new Label(PRO_TEAM);
        
        firstNameTextField = new TextField();
        lastNameTextField = new TextField();
        proTeamComboBox = new ComboBox();
        
        proTeamComboBox.getItems().addAll("ATL", "AZ", "CHC", "CIN", "COL", "LAD",
                "MIA", "MIL", "NYM", "PHI", "PIT", "SD", "SF", "STL", "WAS");
        
        checkBoxHBox = new HBox();
        
        catcher = new CheckBox("C");
        firstBase = new CheckBox("1B");
        secondBase = new CheckBox("2B");
        thirdBase = new CheckBox("3B");
        shortStop = new CheckBox("SS");
        outField = new CheckBox("OF");
        pitcher = new CheckBox("P");
        
        newplayer = new Player();
                
        firstNameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            newplayer.setFirstName(newValue);
        });
        lastNameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            newplayer.setLastName(newValue);
        });
        proTeamComboBox.setOnAction(e -> {
            newplayer.setTeam(proTeamComboBox.getSelectionModel().getSelectedItem().toString());
        });
        catcher.setOnAction(e -> {
            if(!newplayer.getPosition().isEmpty()) {
                newplayer.setPosition(newplayer.getPosition().concat("_" + catcher.getText()));
            }
            else {
               newplayer.setPosition(catcher.getText());
            }
        });
        firstBase.setOnAction(e -> {
            if(!newplayer.getPosition().isEmpty()) {
                newplayer.setPosition(newplayer.getPosition().concat("_" + firstBase.getText()));
            }
            else {
               newplayer.setPosition(firstBase.getText());
            }            
        });        
        secondBase.setOnAction(e -> {
            if(!newplayer.getPosition().isEmpty()) {
                newplayer.setPosition(newplayer.getPosition().concat("_" + secondBase.getText()));
            }
            else {
               newplayer.setPosition(secondBase.getText());
            }            
        });   
        thirdBase.setOnAction(e -> {
            if(!newplayer.getPosition().isEmpty()) {
                newplayer.setPosition(newplayer.getPosition().concat("_" + thirdBase.getText()));
            }
            else {
               newplayer.setPosition(thirdBase.getText());
            }            
        });
        shortStop.setOnAction(e -> {
            if(!newplayer.getPosition().isEmpty()) {
                newplayer.setPosition(newplayer.getPosition().concat("_" + shortStop.getText()));
            }
            else {
               newplayer.setPosition(shortStop.getText());
            }            
        });           
        outField.setOnAction(e -> {
            if(!newplayer.getPosition().isEmpty()) {
                newplayer.setPosition(newplayer.getPosition().concat("_" + outField.getText()));
            }
            else {
               newplayer.setPosition(outField.getText());
            }            
        });
        pitcher.setOnAction(e -> {
            if(!newplayer.getPosition().isEmpty()) {
                newplayer.setPosition(newplayer.getPosition().concat("_" + pitcher.getText()));
            }
            else {
               newplayer.setPosition(pitcher.getText());
            }            
        });           
        // AND FINALLY, THE BUTTONS
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);
        
        // REGISTER EVENT HANDLERS FOR OUR BUTTONS
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            AddPlayerDialog.this.selection = sourceButton.getText();
            AddPlayerDialog.this.hide();
        };
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);

        // NOW LET'S ARRANGE THEM ALL AT ONCE
        gridPane.add(headingLabel, 0, 0, 2, 1);
        gridPane.add(firstNameLabel, 0, 1, 1, 1);
        gridPane.add(firstNameTextField, 1, 1, 1, 1);
        gridPane.add(lastNameLabel, 0, 2, 1, 1);
        gridPane.add(lastNameTextField, 1, 2, 1, 1);
        gridPane.add(proTeamLabel, 0, 3, 1, 1);
        gridPane.add(proTeamComboBox, 1, 3, 1, 1);
        gridPane.addRow(4, catcher, firstBase, secondBase, thirdBase, shortStop,
                outField, pitcher);
        gridPane.add(completeButton, 0, 6, 1, 1);
        gridPane.add(cancelButton, 1, 6, 1, 1);

        // AND PUT THE GRID PANE IN THE WINDOW
        dialogScene = new Scene(gridPane);
        this.setScene(dialogScene);
    }
    
    /**
     * Accessor method for getting the selection the user made.
     * 
     * @return Either YES, NO, or CANCEL, depending on which
     * button the user selected when this dialog was presented.
     */
    public String getSelection() {
        return selection;
    }
    
    public boolean wasCompleteSelected() {
        return selection.equals(COMPLETE);
    }
    
    public void showAddPlayerDialog() {
        // SET THE DIALOG TITLE
        setTitle(ADD_Player_TITLE);
               
        // AND OPEN IT UP
        this.showAndWait();
    }
    
    public Player getPlayer() {
        return this.newplayer;
    }
}