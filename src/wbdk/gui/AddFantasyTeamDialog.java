package wbdk.gui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import wbdk.data.FantasyTeam;

/**
 *
 * @author McKillaGorilla
 */
public class AddFantasyTeamDialog extends Stage {
    // THIS IS THE OBJECT DATA BEHIND THIS UI
    FantasyTeam newFantasyTeam;
    
    // GUI CONTROLS FOR OUR DIALOG
    GridPane gridPane;
    Scene dialogScene;
    Label headingLabel;
    Label teamNameLabel;
    TextField teamNameTextField;
    Label ownerNameLabel;
    TextField ownerNameTextField;;
    Button completeButton;
    Button cancelButton;
    
    // THIS IS FOR KEEPING TRACK OF WHICH BUTTON THE USER PRESSED
    String selection;
    
    // CONSTANTS FOR OUR UI
    public static final String COMPLETE = "Complete";
    public static final String CANCEL = "Cancel";
    public static final String TEAM_HEADING = "Fantasy Team Details";
    public static final String ADD_TEAM_TITLE = "Add New Fantasy Team";
    public static final String TEAM_NAME = "Name: ";
    public static final String OWNER_NAME = "Owner: ";
    /**
     * Initializes this dialog so that it can be used for either adding
     * new schedule items or editing existing ones.
     * 
     * @param primaryStage The owner of this modal dialog.
     */
    public AddFantasyTeamDialog(Stage primaryStage, MessageDialog messageDialog) {       
        // MAKE THIS DIALOG MODAL, MEANING OTHERS WILL WAIT
        // FOR IT WHEN IT IS DISPLAYED
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        // FIRST OUR CONTAINER
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        // PUT THE HEADING IN THE GRID, NOTE THAT THE TEXT WILL DEPEND
        // ON WHETHER WE'RE ADDING OR EDITING
        headingLabel = new Label(TEAM_HEADING);
    
        // NOW THE CRITERIA
        teamNameLabel = new Label(TEAM_NAME);
        ownerNameLabel = new Label(OWNER_NAME);
        
        teamNameTextField = new TextField();
        ownerNameTextField = new TextField();
        
        newFantasyTeam = new FantasyTeam();
       
        teamNameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            newFantasyTeam.setName(newValue);
        });
        ownerNameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            newFantasyTeam.setOwner(newValue);
        });
   
        // AND FINALLY, THE BUTTONS
        completeButton = new Button(COMPLETE);
        cancelButton = new Button(CANCEL);
        
        // REGISTER EVENT HANDLERS FOR OUR BUTTONS
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            AddFantasyTeamDialog.this.selection = sourceButton.getText();
            AddFantasyTeamDialog.this.hide();
        };
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);

        // NOW LET'S ARRANGE THEM ALL AT ONCE
        gridPane.add(headingLabel, 0, 0, 2, 1);
        gridPane.add(teamNameLabel, 0, 1, 1, 1);
        gridPane.add(teamNameTextField, 1, 1, 1, 1);
        gridPane.add(ownerNameLabel, 0, 2, 1, 1);
        gridPane.add(ownerNameTextField, 1, 2, 1, 1);
        gridPane.add(completeButton, 0, 3, 1, 1);
        gridPane.add(cancelButton, 1, 3, 1, 1);

        // AND PUT THE GRID PANE IN THE WINDOW
        dialogScene = new Scene(gridPane);
        this.setScene(dialogScene);
    }
    
    /**
     * Accessor method for getting the selection the user made.
     * 
     * @return Either YES, NO, or CANCEL, depending on which
     * button the user selected when this dialog was presented.
//     */
    public String getSelection() {
        return selection;
    }
    
    public boolean wasCompleteSelected() {
        return selection.equals(COMPLETE);
    }
    
    public void showAddFantasyTeamDialog() {
        // SET THE DIALOG TITLE
        setTitle(ADD_TEAM_TITLE);
               
        // AND OPEN IT UP
        this.showAndWait();
    }
    
    public FantasyTeam getFantasyTeam() {
        return this.newFantasyTeam;
    }
}