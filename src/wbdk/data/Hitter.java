/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk.data;

import javafx.collections.ObservableList;

/**
 *
 * @author Nicolas
 */
public class Hitter extends Player {
    
    private int runs;
    private int homeruns;
    private int runsBattedIn;
    private int stolenBases;
    private double battingAvg;
    //private ObservableList<String> qualifyingPos;
    //if necessary, inclue set and get for ^^^
    
    public void setRuns(int r) {
        this.runs = r;
    }
    
    public void setHomeruns(int hr) {
        this.homeruns = hr;
    }
    
    public void setRunsBattedIn(int rbi) {
        this.runsBattedIn = rbi;
    }
    
    public void setStolenBases(int sb) {
        this.stolenBases = sb;
    }
    
    public void setBattingAvg(double avg) {
        this.battingAvg = avg;
    }
    
    public int getRuns() {
        return this.runs;
    }
    
    public int getHomeRuns() {
        return this.homeruns;
    }
    
    public int getRunsBattedIn() {
        return this.runsBattedIn;
    }
    
    public int getStolenBases() {
        return this.stolenBases;
    }
    
    public double getBattingAvg() {
        return this.battingAvg;
    }
    
}
