/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk.data;

/**
 *
 * @author Nicolas
 */
public class Pitcher extends Player {
    
    private int walks;
    private int strikeouts;
    private int saves;
    private double earnedRunAvg;
    private double walksHitsInningsPitched;
    private String role;
    
    public void setWalks(int w) {
        this.walks = w;
    }
    
    public void setStrikeouts(int s) {
        this.strikeouts = s;
    }
    
    public void setSaves(int s) {
        this.saves = s;
    }
    
    public void setERA(double era) {
        this.earnedRunAvg = era;
    }
    
    public void setWHIP(double whip) {
        this.walksHitsInningsPitched = whip;
    }
    
    public void setRole(String r) {
        this.role = r;
    }
    
    public int getWalks() {
        return this.walks;
    }
    
    public int getStrikeouts() {
        return this.strikeouts;
    }
    
    public int getSaves() {
        return this.saves;
    }
    
    public double getEarnedRunAvg() {
        return this.earnedRunAvg;
    }
    
    public double getWalksHitsInningsPitched() {
        return this.walksHitsInningsPitched;
    }
    
    public String getRole() {
        return this.role;
    }
    
}
