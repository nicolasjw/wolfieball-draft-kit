/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk.data;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Nicolas
 */
public class Team {
    
    private String name;
    private String owner;
    private ObservableList<Player> startingPlayers;
    private ObservableList<Player> taxiPlayers;
    
    public Team() {
        name = new String();
        owner = new String();
        startingPlayers = FXCollections.observableArrayList();
        taxiPlayers = FXCollections.observableArrayList();
    }
    
    public void setName(String n) {
        this.name = n;
    }
    
    public void setOwner(String o) {
        this.owner = o;
    }
    
    public void setStartingList(ObservableList<Player> players) {
        this.startingPlayers.setAll(players);
    }  
    
    public void setTaxiList(ObservableList<Player> players) {
        this.taxiPlayers = players;
    }    
    
    public String getName() {
        return this.name;
    }
    
    public String getOwner() {
        return this.owner;
    }
    
    public ObservableList getStartingPlayer() {
        return this.startingPlayers;
    }
    
    public ObservableList getTaxiPlayes() {
        return this.taxiPlayers;
    }
        
}
