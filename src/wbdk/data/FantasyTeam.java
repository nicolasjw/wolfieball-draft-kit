/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk.data;

import java.util.ArrayList;

/**
 *
 * @author Nicolas
 */
public class FantasyTeam extends Team {
    
    private int playersNeeded = 23;
    private String[] playerSortCheckArray;
    private int budget;
    private int runs;
    private int homeruns;
    private int runsBattedIn;
    private int stolenBases;
    private int walks;
    private int strikeouts;
    private int saves;
    private int totalPoints;
    private double walksHitsInningsPitched;    
    private double battingAvg;
    
    
    public FantasyTeam() {
        playerSortCheckArray = new String[] {"C", "C", "1B", "3B", "CI", "2B", 
            "SS", "MI", "OF", "OF", "OF", "OF", "OF", "U", "P", "P", "P", "P", 
            "P", "P", "P", "P", "P"};
    }
    
    public void setBudget(int amount) {
        this.budget = amount;
    }
    
    public void setRuns(int r) {
        this.runs = r;
    }
    
    public void setHomeruns(int hr) {
        this.homeruns = hr;
    }
    
    public void setRunsBattedIn(int rbi) {
        this.runsBattedIn = rbi;
    }
    
    public void setStolenBases(int sb) {
        this.stolenBases = sb;
    }
    
    public void setWalks(int w) {
        this.walks = w;
    }
    
    public void setStrikeouts(int so) {
        this.strikeouts = so;
    }
    
    public void setSaves(int sv) {
        this.saves = sv;
    }
    
    public void setPoints(int pts) {
        this.totalPoints = pts;
    }
    
    public void setWHIP(double whip) {
        this.walksHitsInningsPitched = whip;
    }
    
    public void setBattingAvg(double avg) {
        this.battingAvg = avg;
    }
    
    public int getPlayersNeeded() {
        return this.playersNeeded;
    }
    
    public int getBudget() {
        return this.budget;
    }
    
    public int getRuns() {
        return this.runs;
    }
    
    public int getHomeruns() {
        return this.homeruns;
    }
    
    public int getRBI() {
        return this.runsBattedIn;
    }
    
    public int getStolenBases() {
        return this.stolenBases;
    }
    
    public int getWalks() {
        return this.walks;
    }
    
    public int getStrikeouts() {
        return this.strikeouts;
    }
    
    public int getSaves() {
        return this.saves;
    }
    
    public int getPoints() {
        return this.totalPoints;
    }
    
    public double getWHIP() {
        return this.walksHitsInningsPitched;
    }
    
    public double getBattingAvg() {
        return this.battingAvg;
    }
    
    public void createOPenPos() {
        this.playerSortCheckArray = new String[] {"C", "C", "1B", "3B", "CI", "2B", 
            "SS", "MI", "OF", "OF", "OF", "OF", "OF", "U", "P", "P", "P", "P", 
            "P", "P", "P", "P", "P"};
    }
    
    public String[] getOpenPositions() {
        return this.playerSortCheckArray;
    }
    
}
