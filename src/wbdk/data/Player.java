package wbdk.data;

import javafx.scene.image.Image;

/**
 *
 * @author Nicolas
 */
public class Player {
    
    private String firstName;
    private String lastName;
    private String position;
    private String chosenPosition;
    private String contract;
    private int yearOfBirth;
    private double salary;
    private String team;
    private String notes;
    private int value;
    private Image nation;
    private Image photo;
    
    
    public Player() {
        firstName = new String();
        lastName = new String();
        position = new String();
        contract = new String();
    }
    
    public void setFirstName(String name) {
        this.firstName = name;
    }
    
    public void setLastName(String name) {
        this.lastName = name;
    }
    
    public void setPosition(String pos) {
        this.position = pos;
    }
    
    public void setChosePosition(String pos) {
        this.chosenPosition = pos;
    }
    
    public void setContract(String contract) {
        this.contract = contract;
    }
    
    public void setYearOfBirth(int year) {
        this.yearOfBirth = year;
    }
    
    public void setSalary(double amount) {
        this.salary = amount;
    }
    
    public void setTeam(String t) {
        this.team = t;
    }
    
    public void setNotes(String n) {
        this.notes = n;
    }
    
    public void setValue(int v) {
        this.value = v;
    }
    
    public void setFlag(Image flag) {
        this.nation = flag;
    }
    
    public void setPhoto(Image selfie) {
        this.photo = selfie;
    }
    
    public String getFirstName() {
        return this.firstName;
    }
    
    public String getLastName() {
        return this.lastName;
    }
    
    public String getPosition() {
        return this.position;
    }
    
    public String getChosenPosition() {
        return this.chosenPosition;
    }
    
    public String getContract() {
        return this.contract;
    }
    
    public int getYearOfBirth() {
        return this.yearOfBirth;
    }
    
    public double getSalary() {
        return this.salary;
    }
    
   public String getTeam() {
        return this.team;
    }
   
   public String getNotes() {
       return this.notes;
   }
   
   public int getValue() {
       return this.value;
   }
   
   public Image getFlag() {
       return this.nation;
   }
   
   public Image getPhoto() {
       return this.photo;
   }
    
}
