/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk.data;

import wolfieballdraftkit.WBDK_StartupConstants;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.scene.image.Image;
import javax.json.JsonArray;
import javax.json.JsonObject;
import wbdk.file.JSONFileManager;

/**
 *
 * @author Nicolas
 */
public class DataManager {
    
    private ObservableList<Player> players;
    private ObservableMap<String, ObservableList<Player>> sortPlayers;
    private ObservableMap<String, ObservableList<Player>> mlbTeams;
    private ObservableList<FantasyTeam> fantasyTeams;
    JSONFileManager fileManager;
    
    private static DataManager singleton = null;
    
    private DataManager() {
        players = FXCollections.observableArrayList();
        sortPlayers =  FXCollections.observableHashMap();
        mlbTeams = FXCollections.observableHashMap();
        fantasyTeams = FXCollections.observableArrayList();
    }
    
    public static DataManager getDataManager() {
        if(singleton == null) {
            singleton = new DataManager();
        }
        return singleton;
    }
    
    public void loadHitters(String filePath, String arrayName) {
        fileManager = JSONFileManager.getJSONFileManager();
        try{
            JsonObject obj = fileManager.loadJSONFile(filePath);
            JsonArray tempPlayers1 = obj.getJsonArray("Hitters");
            JsonObject o;
            String[] split;
            String[] newSplit;
            for(int i = 0; i < tempPlayers1.size(); i++) {
                
                Image nation;
                Image photo;
                
                Hitter h = new Hitter();
                o = tempPlayers1.getJsonObject(i);
                
                split = o.get("TEAM").toString().split("\"");
                h.setTeam(split[1]);
                
                split = o.get("LAST_NAME").toString().split("\"");
                h.setLastName(split[1]);
                
                split = o.get("FIRST_NAME").toString().split("\"");
                h.setFirstName(split[1]);
                
                split = o.get("QP").toString().split("\"");
                h.setPosition(split[1]);
                
                split = o.get("R").toString().split("\"");
                h.setRuns(Integer.valueOf(split[1]));
                
                split = o.get("HR").toString().split("\"");
                h.setHomeruns(Integer.valueOf(split[1]));
                
                split = o.get("RBI").toString().split("\"");
                h.setRunsBattedIn(Integer.valueOf(split[1]));
                
                split = o.get("SB").toString().split("\"");
                h.setStolenBases(Integer.valueOf(split[1]));
                
                split = o.get("AB").toString().split("\"");
                newSplit = o.get("H").toString().split("\"");
                h.setBattingAvg(Double.valueOf(newSplit[1])/Double.valueOf(split[1]));
                
                split = o.get("NOTES").toString().split("\"");
                h.setNotes(split[1]);
                
                split = o.get("YEAR_OF_BIRTH").toString().split("\"");
                h.setYearOfBirth(Integer.valueOf(split[1]));
                
                split = o.get("NATION_OF_BIRTH").toString().split("\"");
                nation = new Image("file:" + WBDK_StartupConstants.PATH_FLAGS + split[1] + ".png");
                h.setFlag(nation);
                
                
                photo = new Image("file:" + WBDK_StartupConstants.PATH_PLAYER_PHOTOS + h.getLastName() + h.getFirstName() + ".jpg");
                if(photo.isError()) {
                    photo = new Image("file:" + WBDK_StartupConstants.PATH_PLAYER_PHOTOS + "AAA_PhotoMissing.jpg");                    
                }
                h.setPhoto(photo);                
                
                players.add(h);
                
                System.out.println(players.get(i).getLastName());
            }
            
            System.out.println(players.get(20).getLastName());
            
        }catch(IOException e) {
            
        }
    }
    
    public void loadPitchers(String filePath, String arrayName) {
        fileManager = JSONFileManager.getJSONFileManager();
        try{
            JsonObject obj = fileManager.loadJSONFile(filePath);
            JsonArray tempPlayers1 = obj.getJsonArray("Pitchers");
            JsonObject o;
            String[] split;
            String[] newSplit;
            String[] ipSplit;
            for(int i = 0; i < tempPlayers1.size(); i++) {
                
                Image nation;
                Image photo;
                
                Pitcher p = new Pitcher();
                o = tempPlayers1.getJsonObject(i);
                
                split = o.get("TEAM").toString().split("\"");
                p.setTeam(split[1]);
                
                split = o.get("LAST_NAME").toString().split("\"");
                p.setLastName(split[1]);
                
                split = o.get("FIRST_NAME").toString().split("\"");
                p.setFirstName(split[1]);
                
                p.setPosition("P");
                      
                split = o.get("W").toString().split("\"");
                p.setWalks(Integer.valueOf(split[1]));
                
                split = o.get("SV").toString().split("\"");
                p.setSaves(Integer.valueOf(split[1]));
                
                split = o.get("K").toString().split("\"");
                p.setStrikeouts(Integer.valueOf(split[1]));
                
                split = o.get("ER").toString().split("\"");
                newSplit = o.get("IP").toString().split("\"");
                p.setERA(Double.valueOf(newSplit[1])/Double.valueOf(split[1]));
                
                split = o.get("W").toString().split("\"");
                newSplit = o.get("H").toString().split("\"");
                ipSplit = o.get("IP").toString().split("\"");
                p.setWHIP(((Double.valueOf(split[1])+Double.valueOf(newSplit[1]))/Double.valueOf(ipSplit[1])));
                
                split = o.get("NOTES").toString().split("\"");
                p.setNotes(split[1]);
                
                split = o.get("YEAR_OF_BIRTH").toString().split("\"");
                p.setYearOfBirth(Integer.valueOf(split[1]));
                
                split = o.get("NATION_OF_BIRTH").toString().split("\"");
                nation = new Image("file:" + WBDK_StartupConstants.PATH_FLAGS + split[1] + ".png");
                p.setFlag(nation);
                
                photo = new Image("file:" + WBDK_StartupConstants.PATH_PLAYER_PHOTOS + p.getLastName() + p.getFirstName() + ".jpg");
                if(photo.isError()) {
                    photo = new Image("file:" + WBDK_StartupConstants.PATH_PLAYER_PHOTOS + "AAA_PhotoMissing.jpg");                    
                }                
                p.setPhoto(photo);
                
                players.add(p);
                
            }
            
            
        }catch(IOException e) {
            
        }
    }    
    
    public void putPlayersIntoMap(ObservableList<Player> newPlayers) {
        ObservableList<Player> all = FXCollections.observableArrayList();
        ObservableList<Player> c = FXCollections.observableArrayList();
        ObservableList<Player> fb = FXCollections.observableArrayList();
        ObservableList<Player> cl = FXCollections.observableArrayList();
        ObservableList<Player> tb = FXCollections.observableArrayList();
        ObservableList<Player> sb = FXCollections.observableArrayList();
        ObservableList<Player> ml = FXCollections.observableArrayList();
        ObservableList<Player> ss = FXCollections.observableArrayList();
        ObservableList<Player> of = FXCollections.observableArrayList();
        ObservableList<Player> u = FXCollections.observableArrayList();
        ObservableList<Player> p = FXCollections.observableArrayList();
        String pos;
        String[] split;
        Player player;
        for(int i = 0; i < newPlayers.size(); i++) {
            player = newPlayers.get(i);
            pos = player.getPosition();
            if(pos.length() >= 3) { //IF PLAYER CAN PLAY MORE THAN 1 POSITION
                split = pos.split("_");
                for(int j = 0; j < split.length-1; j++) { //RUN THROUGH ALL THEIR POSITIONS
                    if(((split[j].equals("2B") || split[j].equals("SS")) &&
                            (split[j+1].equals("2B") || split[j+1].equals("SS")))){
                        ml.add(player);
                    }
                    if(((split[j].equals("1B") || split[j].equals("3B")) &&
                            (split[j+1].equals("1B") || split[j+1].equals("3B")))) {
                        cl.add(player);
                    }
                }
            }
            if(pos.equals("C")) {
                c.add(player);
            }
            if(pos.equals("1B")) {
                fb.add(player);
            }
            if(pos.equals("3B")) {
                tb.add(player);
            }
            if(pos.equals("2B")) {
                sb.add(player);
            }
            if(pos.equals("SS")) {
                ss.add(player);
            }
            if(pos.equals("OF")) {
                of.add(player);
            }
            if(player.getClass().isInstance(new Hitter())) {
                u.add(player);
            }
            if(pos.equals("P")) {
                p.add(player);
            }
        }
        all = newPlayers;
        sortPlayers.put("ALL", all);
        sortPlayers.put("C", c);
        sortPlayers.put("1B", fb);
        sortPlayers.put("Cl", cl);
        sortPlayers.put("3B", tb);
        sortPlayers.put("2B", sb);
        sortPlayers.put("Ml", ml);
        sortPlayers.put("SS", ss);
        sortPlayers.put("OF", of);
        sortPlayers.put("U", u);
        sortPlayers.put("P", p);
    }
    
    public void putMLBPlayerIntoMap() {
        ObservableList<Player> atl = FXCollections.observableArrayList();
        ObservableList<Player> az = FXCollections.observableArrayList();
        ObservableList<Player> chc = FXCollections.observableArrayList();
        ObservableList<Player> cin = FXCollections.observableArrayList();
        ObservableList<Player> col = FXCollections.observableArrayList();
        ObservableList<Player> lad = FXCollections.observableArrayList();
        ObservableList<Player> mia = FXCollections.observableArrayList();
        ObservableList<Player> mil = FXCollections.observableArrayList();
        ObservableList<Player> nym = FXCollections.observableArrayList();
        ObservableList<Player> phi = FXCollections.observableArrayList();
        ObservableList<Player> pit = FXCollections.observableArrayList();        
        ObservableList<Player> sd = FXCollections.observableArrayList();
        ObservableList<Player> sf = FXCollections.observableArrayList();
        ObservableList<Player> stl = FXCollections.observableArrayList();
        ObservableList<Player> was = FXCollections.observableArrayList();
     
        for(int i = 0; i < players.size(); i++) {
            
            Player playerToAdd = players.get(i);
            String playersTeamName = playerToAdd.getTeam();
            
            switch(playersTeamName) {
                case "ATL":
                    atl.add(playerToAdd);
                    break;
                case "AZ":
                    az.add(playerToAdd);
                    break;
                case "CHC":
                    chc.add(playerToAdd);
                    break;
                case "CIN":
                    cin.add(playerToAdd);
                    break;
                case "COL":
                    col.add(playerToAdd);
                    break;
                case "LAD":
                    lad.add(playerToAdd);
                    break;
                case "MIA":
                    mia.add(playerToAdd);
                    break;
                case "MIL":
                    mil.add(playerToAdd);
                    break;
                case "NYM":
                    nym.add(playerToAdd);
                    break;
                case "PHI":
                    phi.add(playerToAdd);
                    break;
                case "PIT":
                    pit.add(playerToAdd);
                    break;
                case "SD":
                    sd.add(playerToAdd);
                    break;
                case "SF":
                    sf.add(playerToAdd);
                    break;
                case "STL":
                    stl.add(playerToAdd);
                    break;
                case "WAS":
                    was.add(playerToAdd);
                    break;
                    
            }
            
            Comparator<Player> comp = new Comparator<Player>() {

                @Override
                public int compare(Player o1, Player o2) {
                    return o1.getLastName().compareTo(o2.getLastName());
                }
            };
            
            atl.sort(comp);
            az.sort(comp);
            chc.sort(comp);
            cin.sort(comp);
            col.sort(comp);
            lad.sort(comp);
            mia.sort(comp);
            mil.sort(comp);
            nym.sort(comp);
            phi.sort(comp);
            pit.sort(comp);
            sd.sort(comp);
            sf.sort(comp);
            stl.sort(comp);
            was.sort(comp);
            
            mlbTeams.put("ATL", atl);
            mlbTeams.put("AZ", az);
            mlbTeams.put("CHC", chc);
            mlbTeams.put("CIN", cin);
            mlbTeams.put("COL", col);
            mlbTeams.put("LAD", lad);
            mlbTeams.put("MIA", mia);
            mlbTeams.put("MIL", mil);
            mlbTeams.put("NYM", nym);
            mlbTeams.put("PHI", phi);
            mlbTeams.put("PIT", pit);
            mlbTeams.put("SD", sd);
            mlbTeams.put("SF", sf);
            mlbTeams.put("STL", stl);
            mlbTeams.put("WAS", was);
            
            
        }
        
    }
    
    public void updateTeamStats() {
        Object player = new Object();
        //GO THROUGH EVERY FANTASY TEAM
        for(int i = 0; i < fantasyTeams.size(); i++) {
            
            FantasyTeam team = fantasyTeams.get(i);
            int playersOnTeam = 0;
            int playersNeeded = 23;
            int moneyLeft = 260;
            int moneyPerPlayer = 0;
            int runs = 0;
            int homeruns = 0;
            int rbi = 0;
            int sb = 0;
            double ba = 0;
            int walks = 0;
            int saves = 0;
            int strikeouts = 0;
            double era = 0;
            double whip = 0;
            int nullCount = 0;
           
            //GO THROUGH EVERY PLAYERS ON THAT TEAM AND TALLY UP THE STATS
            for(int j = 0; j < team.getStartingPlayer().size(); j++) {
            

                
            player = team.getStartingPlayer().get(j);
            if(player instanceof Hitter) { 
                Hitter newP = (Hitter)player;
                System.out.println(newP.getLastName());
            }
            
            if(player instanceof Hitter) {
                Hitter typePlayer = (Hitter)player;
                if(typePlayer.getFirstName().isEmpty()) {
                    nullCount++;
                    continue;
                }
                runs += typePlayer.getRuns();
                homeruns += typePlayer.getHomeruns();
                rbi += typePlayer.getRunsBattedIn();
                sb += typePlayer.getStolenBases();
                ba += typePlayer.getBattingAvg(); //divide by home many players after
                playersNeeded--;
                moneyLeft -= typePlayer.getSalary();
                moneyPerPlayer += typePlayer.getSalary(); //WE WILL DIVIDE ALL THIS BY AMOUNT OF PLAYERS
                playersOnTeam++;                
            } else if (player instanceof Pitcher) {
                Pitcher typePlayer = (Pitcher)player;
                if(typePlayer.getFirstName().isEmpty()) {
                    nullCount++;
                    continue;
                }
                walks += typePlayer.getWalks();
                saves += typePlayer.getSaves();
                strikeouts += typePlayer.getStrikeouts();
                era += typePlayer.getEarnedRunAvg(); //divide by amount of players
                whip += typePlayer.getWalksHitsInningsPitched(); //divide by amount of players
                playersNeeded--;
                moneyLeft -= typePlayer.getSalary();
                moneyPerPlayer += typePlayer.getSalary(); //WE WILL DIVIDE ALL THIS BY AMOUNT OF PLAYERS
                playersOnTeam++;            
            }         
            else if (player instanceof Player) {
                nullCount++;
            }
            }
            System.out.print(nullCount);
            if(nullCount == 23) { //if there are no players
            //reset to default
            team.setPlayersNeeded(playersNeeded);
            team.setMoneyLeft(moneyLeft);
            team.setMoneyPerPlayer(moneyPerPlayer);
            team.setRuns(runs);
            team.setHomeruns(homeruns);
            team.setRunsBattedIn(rbi);
            team.setStolenBases(sb);
            team.setBattingAvg(ba);
            team.setWalks(walks);
            team.setSaves(saves);
            team.setStrikeouts(strikeouts);
            team.setERA(era);
            team.setWHIP(whip);            
            } 
            else {         
            team.setPlayersNeeded(playersNeeded);
            team.setMoneyLeft(moneyLeft);
            team.setMoneyPerPlayer(moneyPerPlayer/playersOnTeam);
            team.setRuns(runs);
            System.out.print(team.getRuns());
            team.setHomeruns(homeruns);
            team.setRunsBattedIn(rbi);
            team.setStolenBases(sb);
            team.setBattingAvg(ba/playersOnTeam);
            team.setWalks(walks);
            team.setSaves(saves);
            team.setStrikeouts(strikeouts);
            team.setERA(era/playersOnTeam);
            team.setWHIP(whip/playersOnTeam);
            }
            
        }
    }
    
    public void updatePoints() {
        ObservableList<FantasyTeam> teams = fantasyTeams;
        
        Comparator<FantasyTeam> runsComp = new Comparator<FantasyTeam>() {
            @Override
            public int compare(FantasyTeam o1, FantasyTeam o2) {
                return String.valueOf(o1.getRuns()).compareTo(String.valueOf(o2.getRuns()));
            }            
        };
        Comparator<FantasyTeam> homerunsComp = new Comparator<FantasyTeam>() {
            @Override
            public int compare(FantasyTeam o1, FantasyTeam o2) {
                return String.valueOf(o1.getHomeruns()).compareTo(String.valueOf(o2.getHomeruns()));
            }            
        };
        Comparator<FantasyTeam> rbiComp = new Comparator<FantasyTeam>() {
            @Override
            public int compare(FantasyTeam o1, FantasyTeam o2) {
                return String.valueOf(o1.getRunsBattedIn()).compareTo(String.valueOf(o2.getRunsBattedIn()));
            }            
        };        
        Comparator<FantasyTeam> stolenBasesComp = new Comparator<FantasyTeam>() {
            @Override
            public int compare(FantasyTeam o1, FantasyTeam o2) {
                return String.valueOf(o1.getStolenBases()).compareTo(String.valueOf(o2.getStolenBases()));
            }            
        };
        Comparator<FantasyTeam> battingAvgComp = new Comparator<FantasyTeam>() {
            @Override
            public int compare(FantasyTeam o1, FantasyTeam o2) {
                return String.valueOf(o1.getBattingAvg()).compareTo(String.valueOf(o2.getBattingAvg()));
            }            
        };
        Comparator<FantasyTeam> walksComp = new Comparator<FantasyTeam>() {
            @Override
            public int compare(FantasyTeam o1, FantasyTeam o2) {
                return String.valueOf(o1.getWalks()).compareTo(String.valueOf(o2.getWalks()));
            }            
        };     
        Comparator<FantasyTeam> savesComp = new Comparator<FantasyTeam>() {
            @Override
            public int compare(FantasyTeam o1, FantasyTeam o2) {
                return String.valueOf(o1.getSaves()).compareTo(String.valueOf(o2.getSaves()));
            }            
        };    
        Comparator<FantasyTeam> strikeoutComp = new Comparator<FantasyTeam>() {
            @Override
            public int compare(FantasyTeam o1, FantasyTeam o2) {
                return String.valueOf(o1.getStrikeouts()).compareTo(String.valueOf(o2.getStrikeouts()));
            }            
        };       
        Comparator<FantasyTeam> eraComp = new Comparator<FantasyTeam>() {
            @Override
            public int compare(FantasyTeam o1, FantasyTeam o2) {
                return String.valueOf(o1.getEarnedRunAvg()).compareTo(String.valueOf(o2.getEarnedRunAvg()));
            }            
        };    
        Comparator<FantasyTeam> whipComp = new Comparator<FantasyTeam>() {
            @Override
            public int compare(FantasyTeam o1, FantasyTeam o2) {
                return String.valueOf(o1.getWalks()).compareTo(String.valueOf(o2.getWalksHitsInningsPitched()));
            }            
        };        
        
        for(int q = 0; q < teams.size(); q++) {
            //normalize points
            teams.get(q).setPoints(0);
        }
        
        //RUN THROUGH EVERY STAT
        for(int i = 0; i < 10; i++) {
        int maxPointsPerCategory = teams.size();            
            switch(i) {
                case 0:
                    teams.sort(runsComp);
                    break;
                case 1:
                    teams.sort(homerunsComp);
                    break;
                case 2:
                    teams.sort(rbiComp);
                    break;
                case 3:
                    teams.sort(stolenBasesComp);
                    break;
                case 4:
                    teams.sort(battingAvgComp);
                    break;
                case 5:
                    teams.sort(walksComp);
                    break;
                case 6:
                    teams.sort(savesComp);
                    break;
                case 7:
                    teams.sort(strikeoutComp);
                    break;
                case 8:
                    teams.sort(eraComp);
                    break;
                case 9:
                    teams.sort(whipComp);
                    break;
            }
            for(int j = teams.size()-1; j >= 0; j--) {
                teams.get(j).setPoints(teams.get(j).getTotalPoints() + maxPointsPerCategory);
                maxPointsPerCategory--;
            }
        }
    }
    
    public void printPlayers() {
        for(int i = 0; i < players.size(); i++) {
            System.out.println(this.players.get(i).getLastName()); {
            
        }
        }
    }
    
    public void setPlayers(ObservableList p) {
        this.players = p;
    }
    
    public void setMLBTeams(ObservableList mlb) {
        this.players = mlb;
    }
    
    public void setFantasyTeams(ObservableList ft) {
        this.fantasyTeams.setAll(ft);
    }
    
    public ObservableList getPlayers() {
        return this.players;
    }
    
    public ObservableMap getSortPlayersMap() {
        return this.sortPlayers;
    }
    
    public ObservableMap getMLBTeams() {
        return this.mlbTeams;
    }
    
    public ObservableList getFantasyTeams() {
        return this.fantasyTeams;
    }
    
}
